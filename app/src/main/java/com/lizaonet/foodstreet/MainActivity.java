package com.lizaonet.foodstreet;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;

import com.aries.ui.widget.alert.UIAlertView;
import com.lengxiaocai.tabbar.NavMenuLayout;
import com.lizaonet.foodstreet.base.BaseActivity;
import com.lizaonet.foodstreet.module.atention.AttentionFrg;
import com.lizaonet.foodstreet.module.menu.MenuFrg;
import com.lizaonet.foodstreet.module.recommend.RecommendFrg;
import com.lizaonet.foodstreet.module.recommend.model.UpdateEvent;
import com.lizaonet.foodstreet.module.self.SelfFrg;
import com.lizaonet.foodstreet.module.self.event.SelectImgEvent;
import com.lizaonet.foodstreet.module.topic.TopicFrg;
import com.lizaonet.foodstreet.utils.ExitUtils;
import com.lizaonet.foodstreet.views.NoScrollViewPager;
import com.zhihu.matisse.Matisse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.eventbus.EventBus;
import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;


public class MainActivity extends BaseActivity {
    @BindView(R.id.viewpager)
    NoScrollViewPager viewpager;
    @BindView(R.id.nav_layout)
    NavMenuLayout nav_layout;
    private ArrayList<Fragment> listFragment;

    private static final int REQUEST_CODE_CHOOSE = 23;
    private List<String> fileList = new ArrayList<>();

    private int[] iconRes = {R.mipmap.tabbar_icon1, R.mipmap.tabbar_icon2, R.mipmap.tabbar_icon3, R.mipmap.tabbar_icon4, R.mipmap.tabbar_icon5};
    private int[] iconResSelected = {R.mipmap.tabbar_icon1_selected, R.mipmap.tabbar_icon2_selected, R.mipmap.tabbar_icon3_selected, R.mipmap.tabbar_icon4_selected, R.mipmap.tabbar_icon5_selected};
    private String[] tabTextList = {"推荐", "菜谱", "话题", "关注", "我的"};

    @Override
    protected void initViews() {
        mToolbar.setVisibility(View.GONE);
        setSwipeBackEnable(false);
        nav_layout.setIconRes(iconRes)//设置未选中图标
                .setIconResSelected(iconResSelected)//设置选中图标
                .setTextRes(tabTextList)//设置文字
                .setTextSize(10)//指定位置的文字大小
                .setTextColor(Color.parseColor("#999999"))//未选中状态下文字颜色
                .setTextColorSelected(Color.parseColor("#212121"))//选中状态下文字颜色
                .setSelected(0);//设置选中的位置

        //选中的点击事件
        nav_layout.setOnItemSelectedListener(new NavMenuLayout.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position) {
                viewpager.setCurrentItem(position);//选中后切换viwepager
            }
        });

        ConversationListFragment conversationListFragment = new ConversationListFragment();
        Uri uri = Uri.parse("rong://" + MainActivity.this.getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist").appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话，该会话聚合显示
                .build();
        conversationListFragment.setUri(uri);

        listFragment = new ArrayList<>();
        listFragment.add(RecommendFrg.getInstance());
        listFragment.add(MenuFrg.getInstance());
        listFragment.add(TopicFrg.getInstance());
        listFragment.add(AttentionFrg.getInstance());
        listFragment.add(SelfFrg.getInstance());
        viewpager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                nav_layout.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // 判断是否需要退客户端逻辑
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (ExitUtils.getInstance().isExit(this)) {
                finish();
            }
        }
        return false;
    }

    private class MyAdapter extends FragmentPagerAdapter {

        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return listFragment.get(position);
        }

        @Override
        public int getCount() {
            return listFragment.size();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            fileList.clear();
            fileList.addAll(Matisse.obtainPathResult(data));
            EventBus.getDefault().post(new SelectImgEvent(fileList));
        }
    }

    public void onEventMainThread(UpdateEvent event) {
        if (!event.getLink_url().isEmpty()) {
            showUpdate(event.getLink_url());
        }
    }

    private void showUpdate(String url) {
        UIAlertView uiAlertView = new UIAlertView(MainActivity.this);
        uiAlertView.setMessage("有新的版本是否升级?", Gravity.CENTER);
        uiAlertView.setMinHeight(200);
        uiAlertView.setNegativeButton("否", getListener(url));
        uiAlertView.setPositiveButton("是", getListener(url));
        uiAlertView.show();
    }


    @NonNull
    private DialogInterface.OnClickListener getListener(final String url) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case BUTTON_NEGATIVE:
                        break;
                    case BUTTON_POSITIVE:
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        Uri content_url = Uri.parse(url);
                        intent.setData(content_url);
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
            }
        };
    }
}
