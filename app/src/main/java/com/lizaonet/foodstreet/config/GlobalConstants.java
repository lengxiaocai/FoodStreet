package com.lizaonet.foodstreet.config;

/**
 * 全局常量
 */
public interface GlobalConstants {


    String KEY = "key";

    //uesrid
    String RONGIMUID = "rongimuid";
    String UID = "uid";
    String TOKEN = "token";
    String HEAD = "head";

    String USERNAME = "userNmae";

    String PHONE = "phone";
    String LEVEL = "level";
    //客服id
    String KEFUID = "kefuid";
    //分享地址
    String SHARE = "share";
    //帖子需要的分享
    String SHARE_NOTE = "share_note";
    //个人分享
    String SHARE_USER = "share_user";
    String USER_LOGIN_ID = "user_login_id";
    String DEVICEID = "deviceid";

    String ISFIRSTSTART = "isFirstStart";

}
