package com.lizaonet.foodstreet.config;

/**
 * 服务器接口配置
 */
public interface ConfigServerInterface {
    boolean isDebug = true;

    /*===============================HOST================================= */
    String BASE_COM_URL = "http://home.shitiaojie.cn/";
    //成功标识
    String RESULT_SUCCES = "1";
    String LOGIN = BASE_COM_URL + "lt_login.php";
    String HOME = BASE_COM_URL + "lt_index.php";
    String AVATOR = BASE_COM_URL + "lt_user.php";
    String LTNOTE = BASE_COM_URL + "lt_note.php";
    String KEY = "133ff1e10a8b244767ef734fb86f37fd";
    String SHAREURL = BASE_COM_URL + "lt_share.php?uid=";

    String USERINFO = BASE_COM_URL + "index.php?s=/Home/User/ru_get_user";
}
