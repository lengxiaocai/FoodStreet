package com.lizaonet.foodstreet.base;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.app.App;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.LoginAct;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.AccountUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tencent.smtt.sdk.WebSettings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;
import io.rong.eventbus.EventBus;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;

public abstract class BaseWebViewAct extends BaseActivity {
    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    public X5WebView mWebView;

    @Override
    protected void initViews() {
        showTitleLeftBtn();
        initWebView();
        initConfig();
        WebSettings webSetting = mWebView.getSettings();
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setUseWideViewPort(false);
        webSetting.setLoadWithOverviewMode(true);
        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(this);
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(false);
        setKuaYu();
        if (!htmlFileName().isEmpty()) {
            if (!htmlFileName().startsWith("http://")) {
                mWebView.loadUrl("file:///android_asset/appweb/html/" + htmlFileName() + ".html");
            } else {
                mWebView.loadUrl(htmlFileName());
            }
        }
    }

    private void initWebView() {
        mWebView = new X5WebView(this, null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    protected abstract void initConfig();

    private void setKuaYu() {
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    protected abstract String htmlFileName();

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_base_webview;
    }

    protected abstract void changeAct(String paramString);

    protected abstract String callJs(String result);

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public boolean isLogin() {
            return AccountUtil.getInstance().isLogin();
        }

        @JavascriptInterface
        public void changeActivity() {

        }

        @JavascriptInterface
        public void initParams(final String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebviewUtils.getInstance().callJs(callJs(result), mWebView);
                }
            });
        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if ("saveuid".equals(baseResponse.getTo())) {
                PreferenceHelper.putString(GlobalConstants.UID, baseResponse.getUid());
                PreferenceHelper.putString(GlobalConstants.TOKEN, baseResponse.getToken());
                EventBus.getDefault().post(new LoginEvent("loginsuccess"));
                connect(BaseWebViewAct.this, baseResponse.getToken());
                finish();
            }
            if (!baseResponse.getTo().isEmpty() && "close".equals(baseResponse.getTo())) {
                finish();
            }
            if (baseResponse.getTo().equals("nologin")) {
                Intent loginIntent = new Intent(BaseWebViewAct.this, LoginAct.class);
                startActivity(loginIntent);
                return;
            }
            if (!baseResponse.getTo().isEmpty()) {
                changeAct(paramString);
            }
        }

        @JavascriptInterface
        public void resize(final float height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getResources()
                            .getDisplayMetrics().widthPixels, (int) (height * getResources()
                            .getDisplayMetrics().density));
                    params.setMargins(15, 0, 10, 0);
                    mWebView.setLayoutParams(params);
                }
            });
        }
    }

    public void connect(Context context, String token) {
        if (context.getApplicationInfo().packageName.equals(App.getCurProcessName(context.getApplicationContext()))) {
            RongIM connect = RongIM.connect(token, new RongIMClient.ConnectCallback() {
                /**
                 * Token 错误，在线上环境下主要是因为 Token 已经过期，您需要向 App Server 重新请求一个新的 Token
                 */
                @Override
                public void onTokenIncorrect() {
                    //TODO  重新获取Token 存储连接
                    WebviewUtils.getInstance().callJs("getToken()", mWebView);
                }

                /**
                 * 连接融云成功
                 * @param userid 当前 token
                 */
                @Override
                public void onSuccess(String userid) {
                    //连接融云成功以后要调用setCurrentUserInfo方法在融云需要的地方显示当前用户的头像。
                    String username = PreferenceHelper.getString(GlobalConstants.USERNAME, "");
                    String avator = PreferenceHelper.getString(GlobalConstants.HEAD, "");
                    RongIM.getInstance().setCurrentUserInfo(new UserInfo(userid, username, Uri.parse(avator)));
                    //为了显示陌生人的头像，携带到消息中
                    RongIM.getInstance().setMessageAttachedUserInfo(true);
                    RongIM.getInstance().enableNewComingMessageIcon(true);//显示新消息提醒
                    RongIM.getInstance().enableUnreadMessageIcon(true);//显示未读消息数目
                    PreferenceHelper.putString(GlobalConstants.RONGIMUID, userid);
                }

                /**
                 * 连接融云失败
                 * @param errorCode 错误码，可到官网 查看错误码对应的注释
                 *                  http://www.rongcloud.cn/docs/android.html#常见错误码
                 */
                @Override
                public void onError(RongIMClient.ErrorCode errorCode) {

                }
            });
        }
    }
}
