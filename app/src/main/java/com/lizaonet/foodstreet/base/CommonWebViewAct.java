package com.lizaonet.foodstreet.base;

public class CommonWebViewAct extends BaseWebViewAct {
    public static String TITLE = "title";
    private String title = "";
    public static String URL = "url";
    private String url = "";

    @Override
    protected void initConfig() {
        title = getIntent().getStringExtra(TITLE);
        url = getIntent().getStringExtra(URL);
        setTitleMiddleText(title);
    }

    @Override
    protected String htmlFileName() {
        return url;
    }

    @Override
    protected void changeAct(String paramString) {

    }

    @Override
    protected String callJs(String result) {
        return null;
    }
}
