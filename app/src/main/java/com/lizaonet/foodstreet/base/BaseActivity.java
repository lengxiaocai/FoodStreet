package com.lizaonet.foodstreet.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.app.App;

import butterknife.ButterKnife;
import io.rong.eventbus.EventBus;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import me.naturs.library.statusbar.StatusBarHelper;


/**
 * Created by lengxiaocai on 2016/2/21.
 * <p>
 * Base基类
 */
public abstract class BaseActivity extends SwipeBackActivity implements View.OnClickListener {

    protected Toolbar mToolbar;
    protected TextView public_title;
    protected RelativeLayout public_title_left;
    protected LinearLayout public_title_right;
    protected LinearLayout ll_public_view;
    protected ImageView iv_public_title_left;
    protected ImageView iv_public_title_right;
    protected TextView tv_public_title_right;
    protected StatusBarHelper mStatusBarHelper;
    protected FrameLayout fl_content;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置为竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //管理Activity
        App.getInstance().addActivity(this);
        //初始化布局
        if (isUseDefaultTitleLayout()) {
            setContentView(R.layout.layout_public_with_title);
            initTitleView();
        } else {
            setContentView(getChildInflateLayout());
        }
        initStatusBar();
        ButterKnife.bind(this);
        setStatusBarShow(true);
        EventBus.getDefault().register(this);
        initViews();
    }

    /**
     * 初始化statusbar
     */
    private void initStatusBar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        if (mToolbar != null) {
            mToolbar.setNavigationIcon(null);
            onTintStatusBar();
            changeColor();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onTintStatusBar() {
        if (mStatusBarHelper == null) {
            mStatusBarHelper = new StatusBarHelper(this, StatusBarHelper.LEVEL_19_TRANSLUCENT, StatusBarHelper.LEVEL_21_VIEW);
        }
        mStatusBarHelper.setColor(getResources().getColor(R.color.appBarColor));
    }

    @Override
    protected void onDestroy() {
        App.getInstance().removeActivity(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.tran_pre_in, R.anim.tran_pre_out);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.tran_next_in, R.anim.tran_next_out);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.tran_next_in, R.anim.tran_next_out);
    }


    /**
     * 显示左边按钮
     */
    protected void showTitleLeftBtn() {
        if (public_title != null) {
            public_title.setVisibility(View.VISIBLE);
        }
        if (public_title_left != null) {
            public_title_left.setVisibility(View.VISIBLE);
            public_title_left.setOnClickListener(this);
        }
    }

    /**
     * 初始化子view
     */
    protected abstract void initViews();

    /**
     * 设置中间title页面中间描述
     */
    protected void setTitleMiddleText(String text) {
        if (public_title != null) {
            public_title.setVisibility(View.VISIBLE);
            public_title.setText(text);
        }

    }

    /**
     * 隐藏中间标题
     */
    protected void hideMiddleTitle() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    /**
     * 隐藏中间标题
     */
    protected void setStatusBarShow(boolean statusBarShow) {
        if (mToolbar != null) {
            if (statusBarShow) {
                mToolbar.setVisibility(View.VISIBLE);
            } else {
                mToolbar.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 显示右边按钮或者文字
     *
     * @param text 要显示的文字，如果为空不显示文字控件
     * @param icon 要显示的图标，如果为0不显示图标
     */
    protected void showTitleRightBtn(String text, int icon) {
        if (public_title != null) {
            public_title.setVisibility(View.VISIBLE);
        }
        if (tv_public_title_right != null) {
            if (TextUtils.isEmpty(text)) {
                tv_public_title_right.setVisibility(View.GONE);
            } else {
                tv_public_title_right.setVisibility(View.VISIBLE);
                tv_public_title_right.setText(text);
            }
        }
        if (iv_public_title_right != null) {
            if (icon == 0) {
                iv_public_title_right.setVisibility(View.GONE);
            } else {
                iv_public_title_right.setVisibility(View.VISIBLE);
                iv_public_title_right.setBackgroundResource(icon);
            }
        }
        if (public_title_right != null) {
            if (!TextUtils.isEmpty(text) || icon != 0) {
                public_title_right.setVisibility(View.VISIBLE);
            } else {
                public_title_right.setVisibility(View.GONE);
            }
            public_title_right.setOnClickListener(this);
        }
    }

    /**
     * 初始化公共布局
     */
    private void initTitleView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        public_title = (TextView) findViewById(R.id.public_title);
        public_title_left = (RelativeLayout) findViewById(R.id.public_title_left);
        iv_public_title_left = (ImageView) findViewById(R.id.iv_public_title_left);
        public_title_right = (LinearLayout) findViewById(R.id.public_title_right);
        iv_public_title_right = (ImageView) findViewById(R.id.iv_public_title_right);
        tv_public_title_right = (TextView) findViewById(R.id.tv_public_title_right);
        ll_public_view = (LinearLayout) findViewById(R.id.ll_public_view);
        if (public_title != null) {
            public_title.setVisibility(View.GONE);
        }
        fl_content = (FrameLayout) findViewById(R.id.fl_content);
        if (fl_content != null) {
            fl_content.addView(LayoutInflater.from(this).inflate(getChildInflateLayout(), null));
        }

    }


    /**
     * 获取子孩子要inflate的布局
     * 不可以返回0
     */
    protected abstract int getChildInflateLayout();


    /**
     * 是否使用默认的顶部布局，默认 true
     * 子类不需要时，复写此方法返回 false 即可
     * 当子类没有调用父类的设置 title等方法时，title 也不会显示
     * 所以如果未调用本类的设置 title相关方法时，尽量复写此方法，并返回 false
     */
    protected boolean isUseDefaultTitleLayout() {
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.public_title_left) {
            finish();
            overridePendingTransition(R.anim.tran_pre_in, R.anim.tran_pre_out);
        }
    }

    /**
     * 设置toolbar颜色
     */
    private void changeColor() {
        String appColor = "#FBBF41";
        mToolbar.setBackgroundColor(Color.parseColor(appColor));
        mStatusBarHelper.setColor(Color.parseColor(appColor));
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(AccountInfo event) {

    }
}
