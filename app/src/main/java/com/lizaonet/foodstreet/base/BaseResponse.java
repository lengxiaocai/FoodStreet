package com.lizaonet.foodstreet.base;

import java.io.Serializable;
import java.util.List;

public class BaseResponse implements Serializable {

    private String to;
    private String age;
    private String sex;
    private String title;
    private String typename;

    public String getChatname() {
        return chatname;
    }

    public void setChatname(String chatname) {
        this.chatname = chatname;
    }

    private String chatname;

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    private String c_id;

    public String getFuid() {
        return fuid;
    }

    public void setFuid(String fuid) {
        this.fuid = fuid;
    }

    private String fuid;

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    private String content;
    private String shareurl;

    public String getShareurl() {
        return shareurl;
    }

    public void setShareurl(String shareurl) {
        this.shareurl = shareurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    private String img;
    /**
     * data : [{"city_id":"110100","city_name":"北京市","selected":true},{"city_id":"120100","city_name":"天津市","selected":true}]
     */

    private String data;


    public String getDownloadurl() {
        return downloadurl;
    }

    public void setDownloadurl(String downloadurl) {
        this.downloadurl = downloadurl;
    }

    private String downloadurl;
    private String buid;

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    private String head;

    public String getChatuid() {
        return chatuid;
    }

    public void setChatuid(String chatuid) {
        this.chatuid = chatuid;
    }

    private String chatuid;
    private List<String> imgurl;

    public String getBuid() {
        return buid;
    }

    public void setBuid(String buid) {
        this.buid = buid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getConcern_type() {
        return concern_type;
    }

    public void setConcern_type(String concern_type) {
        this.concern_type = concern_type;
    }

    public String getYue_province() {
        return yue_province;
    }

    public void setYue_province(String yue_province) {
        this.yue_province = yue_province;
    }

    private String concern_type;
    private String yue_province;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private String nickname;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String index;

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    private String link_url;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String phone;
    private String kefuid;

    public String getKefuid() {
        return kefuid;
    }

    public void setKefuid(String kefuid) {
        this.kefuid = kefuid;
    }

    private int result;

    public int getResult() {
        return result;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public List<UrlBean> getUrl() {
        return url;
    }

    public void setUrl(List<UrlBean> url) {
        this.url = url;
    }

    public void setResult(int result) {
        this.result = result;
    }

    private String funcname;

    private List<UrlBean> url;

    public String getFuncname() {
        return funcname;
    }

    public void setFuncname(String funcname) {
        this.funcname = funcname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private String uid;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    /**
     * to : login
     */

    /**
     * 是否成功
     *
     * @return
     */
    public boolean isSucc() {
        return getResult() == 1;
    }

    public List<String> getImgurl() {
        return imgurl;
    }

    public void setImgurl(List<String> imgurl) {
        this.imgurl = imgurl;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    public static class UrlBean implements Serializable {
        /**
         * index : 0
         * url : http://101.132.102.134/wap/JSON/Forum/Note/2147/1517276042/1.png
         */

        private String index;
        private String url;

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
