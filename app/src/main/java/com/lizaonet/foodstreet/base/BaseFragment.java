package com.lizaonet.foodstreet.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lizaonet.foodstreet.R;

import butterknife.ButterKnife;
import io.rong.eventbus.EventBus;


/**
 * Fragment 基类
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    /**
     * 顶部 title 布局
     */
    protected View view_line;

    protected Toolbar toolbar;
    /**
     * 中间文字
     */
    protected TextView tv_public_title_middle;
    //左边文字
    protected TextView tv_public_title_left;
    protected TextView tv_public_title_right;
    protected RelativeLayout public_title_left;
    protected LinearLayout public_title_right;
    protected ImageView iv_public_title_left;

    /**
     * title 右侧
     */
    protected ImageView iv_public_title_right;


    /**
     * 内容
     */
    protected FrameLayout fl_content;

    protected View mView;

    /**
     * 显示右边按钮
     */
    protected void showTitleRightBtn() {
        iv_public_title_right.setVisibility(View.VISIBLE);
        iv_public_title_right.setOnClickListener(this);
    }

    /**
     * 设置右边按钮图片
     *
     * @param icon 要显示的图标，如果为0不显示图标
     */
    protected void setTitleRightBtn(int icon) {
        if (icon == 0) {
            iv_public_title_right.setVisibility(View.GONE);
        } else {
            iv_public_title_right.setVisibility(View.VISIBLE);
            iv_public_title_right.setImageResource(icon);
            iv_public_title_right.setOnClickListener(this);
        }
    }

    /**
     * 设置中间title页面中间描述
     */
    protected void setTitleMiddleText(String text) {
        tv_public_title_middle.setVisibility(View.VISIBLE);
        tv_public_title_middle.setText(text);
    }

    /**
     * 设置左边文字
     *
     * @param text
     */
    protected void setLeftText(String text) {
        public_title_left.setVisibility(View.VISIBLE);
        iv_public_title_left.setVisibility(View.GONE);
        tv_public_title_left.setVisibility(View.VISIBLE);
        tv_public_title_left.setTextSize(17);
        tv_public_title_left.setText(text);
    }

    /**
     * 设置右边文字
     *
     * @param text
     */
    protected void setRightText(String text) {
        public_title_right.setVisibility(View.VISIBLE);
        iv_public_title_right.setVisibility(View.GONE);
        tv_public_title_right.setVisibility(View.VISIBLE);
        tv_public_title_right.setTextSize(17);
        tv_public_title_right.setText(text);
    }

    /**
     * 设置中间 title 的图片
     *
     * @param icon 要显示的图标，如果为0显示默认 logo图片
     */
    protected void setTitleMiddleIcon(int icon) {
        if (icon != 0) {
            iv_public_title_right.setBackgroundResource(icon);
        }
        tv_public_title_middle.setVisibility(View.GONE);

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.tran_next_in, R.anim.tran_next_out);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        getActivity().overridePendingTransition(R.anim.tran_next_in, R.anim.tran_next_out);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBeforeSetContentView(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
            return mView;
        } else {
            if (isUseDefaultTitleLayout()) {
                mView = inflater.inflate(R.layout.layout_public_frg_with_title, null);
                initTitleView();
            } else {
                mView = inflater.inflate(getChildInflateLayout(), null);
            }
        }
        ButterKnife.bind(this, mView);
        initViews();
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 是否使用默认的顶部布局，默认 true
     * 子类不需要时，复写此方法返回 false 即可
     * 当子类没有调用父类的设置 title等方法时，title 也不会显示
     * 所以如果未调用本类的设置 title相关方法时，尽量复写此方法，并返回 false
     */
    protected boolean isUseDefaultTitleLayout() {
        return true;
    }

    private void initTitleView() {
        if (mView != null) {
            view_line = mView.findViewById(R.id.view_line);
            toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
            tv_public_title_middle = (TextView) mView.findViewById(R.id.public_title);
            tv_public_title_left = (TextView) mView.findViewById(R.id.tv_public_title_left);
            iv_public_title_left = (ImageView) mView.findViewById(R.id.iv_public_title_left);
            public_title_left = (RelativeLayout) mView.findViewById(R.id.public_title_left);
            public_title_right = (LinearLayout) mView.findViewById(R.id.public_title_right);
            iv_public_title_right = (ImageView) mView.findViewById(R.id.iv_public_title_right);
            tv_public_title_right = (TextView) mView.findViewById(R.id.tv_public_title_right);
            fl_content = (FrameLayout) mView.findViewById(R.id.fl_content);
            fl_content.addView(View.inflate(getActivity(), getChildInflateLayout(), null));
            iv_public_title_right.setVisibility(View.GONE);
        }
    }

    /**
     * 获取子孩子要inflate的布局
     * 不可以返回0
     */
    protected abstract int getChildInflateLayout();

    /**
     * 初始化子view
     */
    protected abstract void initViews();

    /**
     * 在setContentView之前初始化数据
     */
    protected void initBeforeSetContentView(Bundle savedInstanceState) {

    }

    public void defaultStartActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.public_title_left) {
            getFragmentManager().popBackStackImmediate();
        }
    }

    public void onEventMainThread(AccountInfo event) {

    }

}
