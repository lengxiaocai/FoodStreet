package com.lizaonet.foodstreet.base;

/**
 * Created by caoxiuyun on 2017/8/2.
 */

public class AccountInfo {
    public String pid = "";

    public AccountInfo(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }
}
