package com.lizaonet.foodstreet.base;

import android.widget.BaseAdapter;

import java.util.List;

/**
 * 简写的BaseAdapter
 */
public abstract class CustomAdapter<T> extends BaseAdapter {
    protected List<T> list;

    public CustomAdapter(List<T> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<T> getList() {
        return list;
    }
}
