package com.lizaonet.foodstreet.utils;

public interface WebViewJavaScriptFunction {

    void onJsFunctionCalled(String tag);
}
