package com.lizaonet.foodstreet.utils;

import android.view.View;
import android.widget.Toast;

import com.lizaonet.foodstreet.app.App;


/**
 * 土司工具类
 */
public enum Tips {
    instance, Tips;
    private View v;
    private Toast it;

    private void init() {
        if (v == null || it == null) {
            v = Toast.makeText(App.getContext(), "", Toast.LENGTH_SHORT).getView();
            it = new Toast(App.getContext());
            it.setView(v);
            it.setView(v);
        }
    }

    public void tipShort(CharSequence content) {
        init();
        it.setDuration(Toast.LENGTH_SHORT);
        it.setText(content);
        it.show();
    }

    public void tipShort(int resId) {
        init();
        it.setText(resId);
        it.setDuration(Toast.LENGTH_SHORT);
        it.show();
    }

    public void tipLong(CharSequence content) {
        init();
        it.setText(content);
        it.setDuration(Toast.LENGTH_LONG);
        it.show();
    }

    public void tipLong(int resId) {
        init();
        it.setText(resId);
        it.setDuration(Toast.LENGTH_LONG);
        it.show();
    }
}
