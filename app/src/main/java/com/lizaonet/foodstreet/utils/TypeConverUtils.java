package com.lizaonet.foodstreet.utils;

/**
 * Created by caoxiuyun on 16/5/23.
 * <p>
 * 对象类型转换工具类
 */

public class TypeConverUtils {

    private static TypeConverUtils instance;

    public static TypeConverUtils getInstance() {
        if (instance == null) {
            synchronized (TypeConverUtils.class) {
                if (instance == null) {
                    instance = new TypeConverUtils();
                }
            }
        }
        return instance;
    }

    /**
     * 对象转Int
     *
     * @param value
     * @param defaultValue
     * @return integer
     */
    public final static int convertToInt(Object value, int defaultValue) {
        if (value == null || "".equals(value.toString().trim())) {
            return defaultValue;
        }
        try {
            return Integer.valueOf(value.toString());
        } catch (Exception e) {
            try {
                return Double.valueOf(value.toString()).intValue();
            } catch (Exception e1) {
                return defaultValue;
            }
        }
    }

    /**
     * 对象转为字符串类型
     *
     * @param value
     * @param defaultValue
     * @return
     */
    public final static String convertToString(Object value, String defaultValue) {
        if (value == null || "".equals(value.toString().trim())) {
            return defaultValue;
        }
        return String.valueOf(value);
    }

    /**
     * 对象转Double
     *
     * @param value
     * @param defaultValue
     * @return
     */
    public final static Double convertToDouble(Object value, Double defaultValue) {
        if (value == null || "".equals(value.toString().trim()) || "0.0".equals(value.toString().trim()) || "0".equals(value.toString().trim())) {
            return defaultValue;
        }
        try {
            return Double.valueOf(value.toString());
        } catch (Exception e) {
            try {
                return Double.valueOf(value.toString());
            } catch (Exception e1) {
                return defaultValue;
            }
        }
    }

    /**
     * 对象转换为Float
     *
     * @param value
     * @param defaultValue
     * @return
     */
    public final static Float convertToFloat(Object value, Float defaultValue) {
        if (value == null || "".equals(value.toString().trim()) || "0.00f".equals(value.toString().trim())) {
            return defaultValue;
        }
        try {
            return Float.valueOf(value.toString());
        } catch (Exception e) {
            try {
                return Float.valueOf(value.toString());
            } catch (Exception e1) {
                return defaultValue;
            }
        }
    }

    /**
     * 对象类型转换为Long类型
     *
     * @param value
     * @param defaultValue
     * @return
     */
    public final static Long convertToLong(Object value, Long defaultValue) {
        if (value == null || "".equals(value.toString().trim())) {
            return defaultValue;
        }
        try {
            return Long.valueOf(value.toString());
        } catch (Exception e) {
            try {
                return Long.valueOf(value.toString());
            } catch (Exception e1) {
                return defaultValue;
            }
        }
    }

}
