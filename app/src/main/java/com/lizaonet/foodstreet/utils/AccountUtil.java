package com.lizaonet.foodstreet.utils;

/**
 * Created by caoxiuyun on 16/4/5.
 */

import android.text.TextUtils;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.config.GlobalConstants;


/**
 * 账户相关工具类
 */
public class AccountUtil {

    private static AccountUtil accountUtil = null;

    private AccountUtil() {
    }

    public static AccountUtil getInstance() {
        if (accountUtil == null) {
            accountUtil = new AccountUtil();
        }
        return accountUtil;
    }

    /**
     * 判断用户是否登录
     *
     * @return
     */
    public boolean isLogin() {
        String userId = PreferenceHelper.getString(GlobalConstants.UID, "");
        return !TextUtils.isEmpty(userId);
    }


    /**
     * 更新UserId信息
     *
     * @param userid
     */
    public void updateAccountUserId(String userid) {
        PreferenceHelper.putString(GlobalConstants.UID, userid);
    }

    /**
     * 更新用户昵称
     *
     * @param userName
     */
    public void updateAccountUserName(String userName) {
        PreferenceHelper.putString(GlobalConstants.USERNAME, userName);
    }

    /**
     * 更新用户头像
     *
     * @param acvtorUrl
     */
    public void updateAccountUserHeader(String acvtorUrl) {
        PreferenceHelper.putString(GlobalConstants.HEAD, acvtorUrl);
    }

    /**
     * 更新用户信息
     *
     * @param result
     */
    public void updateUserInfo(BaseResponse result) {
//        PreferenceHelper.putString(GlobalConstants.HEAD, String.valueOf(result.getData().getAvatar()));
//        PreferenceHelper.putString(GlobalConstants.UID, String.valueOf(result.getData().getUid()));
//        PreferenceHelper.putString(GlobalConstants.PHONE, String.valueOf(result.getData().getPhone()));
//        PreferenceHelper.putString(GlobalConstants.USERNAME, String.valueOf(result.getData().getNickname()));
//        PreferenceHelper.putString(GlobalConstants.LEVEL, String.valueOf(result.getData().getLevel()));
//        PreferenceHelper.putString(GlobalConstants.KEFUID, String.valueOf(result.getData().getKefu()));
//        PreferenceHelper.putString(GlobalConstants.SHARE, String.valueOf(result.getData().getShare()));
//        PreferenceHelper.putString(GlobalConstants.SHARE_NOTE, String.valueOf(result.getData().getShare_note()));
//        PreferenceHelper.putString(GlobalConstants.SHARE_USER, String.valueOf(result.getData().getShare_user()));
//        PreferenceHelper.putString(GlobalConstants.USER_LOGIN_ID, String.valueOf(result.getData().getUser_login_id()));
    }

    public void clearUserInfo() {
        PreferenceHelper.putString(GlobalConstants.HEAD, "");
        PreferenceHelper.putString(GlobalConstants.UID, "");
        PreferenceHelper.putString(GlobalConstants.PHONE, "");
        PreferenceHelper.putString(GlobalConstants.USERNAME, "");
        PreferenceHelper.putString(GlobalConstants.LEVEL, "");
        PreferenceHelper.putString(GlobalConstants.KEFUID, "");
        PreferenceHelper.putString(GlobalConstants.SHARE, "");
        PreferenceHelper.putString(GlobalConstants.SHARE_NOTE, "");
        PreferenceHelper.putString(GlobalConstants.SHARE_USER, "");
        PreferenceHelper.putString(GlobalConstants.USER_LOGIN_ID, "");
    }

}
