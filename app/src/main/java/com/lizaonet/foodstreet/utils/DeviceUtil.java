package com.lizaonet.foodstreet.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * 获取DevcideID
 */

@SuppressLint("MissingPermission")
public class DeviceUtil {
    /**
     * 手机唯一标识
     *
     * @param context
     * @return
     */
    @SuppressLint("MissingPermission")
    public static String getDeviceId(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String uniqueId = deviceUuid.toString();
        return uniqueId;
    }

    public static String getImei(Context context) {
        String str = "";
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            str = "35" + //we make this look like a valid IMEI
                    Build.BOARD.length() % 10 +
                    Build.BRAND.length() % 10 +
                    Build.CPU_ABI.length() % 10 +
                    Build.DEVICE.length() % 10 +
                    Build.DISPLAY.length() % 10 +
                    Build.HOST.length() % 10 +
                    Build.ID.length() % 10 +
                    Build.MANUFACTURER.length() % 10 +
                    Build.MODEL.length() % 10 +
                    Build.PRODUCT.length() % 10 +
                    Build.TAGS.length() % 10 +
                    Build.TYPE.length() % 10 +
                    Build.USER.length() % 10; //13 digits
            String szImei = tm.getDeviceId();
            str = str + szImei;
        } catch (Exception e) {
            //可以加运行时权限
        }
        str = GetMD5Code(str);
        return str;
    }

    public static String GetMD5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    // 全局数组
    private final static String[] strDigits = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }

    // 移动
    private static final int CHINA_MOBILE = 1;
    // 联通
    private static final int UNICOM = 2;
    // 电信
    private static final int TELECOMMUNICATIONS = 3;
    // 失败
    private static final int ERROR = 0;

    /**
     * 手机MAC地址
     *
     * @param context
     * @return
     */
    public static String getMacAddressInfo(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        return info.getMacAddress();
    }

    /**
     * TelephonyManager对象
     *
     * @param context
     * @return
     */
    private static TelephonyManager getTelphoneManager(Context context) {
        return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * DeviceId
     *
     * @param context
     * @return
     */
    public static String getDeviceID(Context context) {
        return getTelphoneManager(context).getDeviceId();
    }

    /**
     * IMSI号
     *
     * @param context
     * @return
     */
    public static String getImis(Context context) {
        return getTelphoneManager(context).getSubscriberId();
    }

    /**
     * 厂商信息
     *
     * @return
     */
    public static String getProductInfo() {
        return Build.MODEL;
    }

    /**
     * release版本
     *
     * @return
     */
    public static String getReleaseVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * SDK_INT 版本
     *
     * @return
     */
    public static int getSDKVersion() {
        return Build.VERSION.SDK_INT;
    }

    /**
     * 手机号码
     *
     * @param context
     * @return
     */
    @SuppressLint("MissingPermission")
    public static String getPhoneNum(Context context) {
        return getTelphoneManager(context).getLine1Number();
    }

    /**
     * 当前运营商
     *
     * @param context
     * @return 返回0 表示失败 1表示为中国移动 2为中国联通 3为中国电信
     */
    public static int getProviderName(Context context) {
        String IMSI = getImis(context);
        if (IMSI == null) {
            return ERROR;
        }
        if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
            return CHINA_MOBILE;
        } else if (IMSI.startsWith("46001")) {
            return UNICOM;
        } else if (IMSI.startsWith("46003")) {
            return TELECOMMUNICATIONS;
        }
        return ERROR;
    }

    /**
     * 手机CPU名字
     *
     * @return
     */
    public static String getCpuName() {
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            // 读取文件CPU信息
            fileReader = new FileReader("/pro/cpuinfo");
            bufferedReader = new BufferedReader(fileReader);
            String string = bufferedReader.readLine();
            String[] strings = string.split(":\\s+", 2);
            return strings[1];
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                }
            }
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    /**
     * 检查程序是否运行
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isAppRunning(Context context, String packageName) {
        boolean isAppRunning = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (info.topActivity.getPackageName().equals(packageName) && info.baseActivity.getPackageName().equals(packageName)) {
                isAppRunning = true;
                // find it, break
                break;
            }
        }
        return isAppRunning;
    }

    /**
     * 是否在最前面
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isTopActivity(Context context, String packageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
        if (tasksInfo.size() > 0) {
            // 应用程序位于堆栈的顶层
            if (packageName.equals(tasksInfo.get(0).topActivity.getPackageName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * 禁止截屏
     *
     * @param activity 所对应的Activity
     */
    public static void refuseScreenshot(Activity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

}
