package com.lizaonet.foodstreet.utils;

import cn.bingoogolapple.refreshlayout.BGARefreshLayout;

/**
 * 刷新管理
 */

public class RefreshUtils {
    /**
     * 停止刷新
     *
     * @param mRefreshLayout
     * @return
     */
    public static void endLoading(BGARefreshLayout mRefreshLayout) {
        mRefreshLayout.endRefreshing();
        mRefreshLayout.endLoadingMore();
    }
}
