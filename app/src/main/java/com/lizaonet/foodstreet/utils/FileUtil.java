package com.lizaonet.foodstreet.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileUtil {
    private static final String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath();

    //保存照片
    public static String saveBitmap(Bitmap b) {
        String jpegName = rootPath + "/" + getTime() + ".jpg";
        try {
            FileOutputStream fout = new FileOutputStream(jpegName);
            BufferedOutputStream bos = new BufferedOutputStream(fout);
            b.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jpegName;
    }

    //获取视频存储路径
    public static String getMediaOutputPath() {
        return rootPath + "/" + getTime() + ".mp4";
    }

    private static String getTime() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date(System.currentTimeMillis()));
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    /**
     * 文件转base64字符串
     *
     * @param file
     * @return
     */
    public static String fileToBase64(File file) {
        String base64 = null;
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            byte[] bytes = new byte[in.available()];
            int length = in.read(bytes);
            base64 = Base64.encodeToString(bytes, 0, length, Base64.DEFAULT);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "base64," + base64;
    }

    public static String fileToBase64(Bitmap bit) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.JPEG, 100, bos);//参数100表示不压缩
        byte[] bytes = bos.toByteArray();
        return "base64," + Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    /**
     * 文件分割
     *
     * @param file
     * @return
     */
    public static List<String> splitFile(File file) {
        List<String> fileStrList = new ArrayList<>();
        FileInputStream inputStream = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] readBuff = new byte[102400];
        int readLen = -1;
        try {
            inputStream = new FileInputStream(file);
            while ((readLen = inputStream.read(readBuff)) != -1) {
                outputStream.write(readBuff, 0, readLen);
                outputStream.flush();
                byte[] bytes = outputStream.toByteArray();
                String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
                fileStrList.add(base64);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileStrList;
    }

    /**
     * byte数组转hex
     *
     * @param data
     * @return
     */
    private static String toHex(byte[] data) {
        StringBuilder strBuffer = new StringBuilder();
        String hexStr = "";

        for (byte aData : data) {
            strBuffer.append(ByteToHex(aData));
        }

        if (strBuffer.length() > 0) {
            hexStr = strBuffer.toString();
        }
        return hexStr;
    }

    /**
     * byte转hex
     *
     * @param a
     * @return
     */
    private static String ByteToHex(byte a) {
        int aaa = (a < 0) ? a + 256 : a;
        StringBuilder sb = new StringBuilder();
        switch (aaa / 16) {
            case 10:
                sb.append("A");
                break;
            case 11:
                sb.append("B");
                break;
            case 12:
                sb.append("C");
                break;
            case 13:
                sb.append("D");
                break;
            case 14:
                sb.append("E");
                break;
            case 15:
                sb.append("F");
                break;
            default:
                sb.append(aaa / 16);
                break;
        }
        switch (aaa % 16) {
            case 10:
                sb.append("A");
                break;
            case 11:
                sb.append("B");
                break;
            case 12:
                sb.append("C");
                break;
            case 13:
                sb.append("D");
                break;
            case 14:
                sb.append("E");
                break;
            case 15:
                sb.append("F");
                break;
            default:
                sb.append(aaa % 16);
                break;
        }
        return sb.toString();
    }


    // 根据路径获得图片并压缩，返回bitmap用于显示
    public static Bitmap getSmallBitmap(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 480, 800);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    //计算图片的缩放值
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    //把bitmap转换成String
    public static String bitmapToString(String filePath) {
        Bitmap bm = getSmallBitmap(filePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //1.5M的压缩后在100Kb以内，测试得值,压缩后的大小=94486字节,压缩后的大小=74473字节
        //这里的JPEG 如果换成PNG，那么压缩的就有600kB这样
        bm.compress(Bitmap.CompressFormat.JPEG, 40, baos);
        byte[] b = baos.toByteArray();
        Log.d("d", "压缩后的大小=" + b.length);
        return Base64.encodeToString(b, Base64.DEFAULT).replaceAll("\n", "");
    }


}
