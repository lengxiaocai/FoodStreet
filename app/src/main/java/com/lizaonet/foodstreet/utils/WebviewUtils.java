package com.lizaonet.foodstreet.utils;

import android.os.Build;

import com.tencent.smtt.sdk.ValueCallback;

public class WebviewUtils {
    private static WebviewUtils webviewUtils = null;

    private WebviewUtils() {
    }

    public static WebviewUtils getInstance() {
        if (webviewUtils == null) {
            webviewUtils = new WebviewUtils();
        }
        return webviewUtils;
    }

    public void callJs(String jsString, X5WebView webView) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            webView.loadUrl("javascript:" + jsString);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {

                    }
                });
            }
        }
    }
}
