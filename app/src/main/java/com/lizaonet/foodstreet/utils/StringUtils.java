package com.lizaonet.foodstreet.utils;

/**
 * Created by caoxiuyun on 16/6/20.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author Caoxiuyun
 */
public final class StringUtils {

    /**
     * 默认的空值
     */
    public static final String EMPTY = "";

    /**
     * 检查字符串是否为空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 检查字符串是否为空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else return str.length() == 0;
    }

    /**
     * 截取并保留标志位之前的字符串
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return
     */
    public static String substringBefore(String str, String expr) {
        if (isEmpty(str) || expr == null) {
            return str;
        }
        if (expr.length() == 0) {
            return EMPTY;
        }
        int pos = str.indexOf(expr);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * 截取并保留标志位之后的字符串
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return
     */
    public static String substringAfter(String str, String expr) {
        if (isEmpty(str)) {
            return str;
        }
        if (expr == null) {
            return EMPTY;
        }
        int pos = str.indexOf(expr);
        if (pos == -1) {
            return EMPTY;
        }
        return str.substring(pos + expr.length());
    }

    /**
     * 截取并保留最后一个标志位之前的字符串
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return
     */
    public static String substringBeforeLast(String str, String expr) {
        if (isEmpty(str) || isEmpty(expr)) {
            return str;
        }
        int pos = str.lastIndexOf(expr);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * 截取并保留最后一个标志位之后的字符串
     *
     * @param str
     * @param expr 分隔符
     * @return
     */
    public static String substringAfterLast(String str, String expr) {
        if (isEmpty(str)) {
            return str;
        }
        if (isEmpty(expr)) {
            return EMPTY;
        }
        int pos = str.lastIndexOf(expr);
        if (pos == -1 || pos == (str.length() - expr.length())) {
            return EMPTY;
        }
        return str.substring(pos + expr.length());
    }

    /**
     * 把字符串按分隔符转换为数组
     *
     * @param string 字符串
     * @param expr   分隔符
     * @return
     */
    public static String[] stringToArray(String string, String expr) {
        return string.split(expr);
    }

    /**
     * 去除字符串中的空格
     *
     * @param str
     * @return
     */
    public static String noSpace(String str) {
        str = str.trim();
        str = str.replace(" ", "_");
        return str;
    }

    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    public static String replaceURL(String str) {
        Matcher m = Pattern.compile("[a-zA-z]+://[^\\s]*").matcher(str);
        while (m.find()) {
            return m.group();
        }
        return null;
    }

    /**
     * 设置不同颜色文本
     *
     * @param str
     * @param startColor
     * @param endColor
     * @return
     */
    public static SpannableStringBuilder setDifferentColorText(String str, int startColor, int endColor) {
        int startIndex = str.indexOf(":") + 1;
        SpannableStringBuilder builder = new SpannableStringBuilder(str);
        ForegroundColorSpan redSpan = new ForegroundColorSpan(startColor);
        ForegroundColorSpan whiteSpan = new ForegroundColorSpan(endColor);
        builder.setSpan(redSpan, 0, startIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(whiteSpan, startIndex, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return builder;
    }

    /**
     * 设置不同颜色文本
     *
     * @param str
     * @param tagStr
     * @param startColor
     * @param endColor
     * @return
     */
    public static SpannableStringBuilder setDifferentColorText(String str, String tagStr, int startColor, int endColor) {
        int startIndex = str.indexOf(tagStr) + 1;
        SpannableStringBuilder builder = new SpannableStringBuilder(str);
        ForegroundColorSpan redSpan = new ForegroundColorSpan(startColor);
        ForegroundColorSpan whiteSpan = new ForegroundColorSpan(endColor);
        if (startIndex >= 0) {
            builder.setSpan(redSpan, 0, startIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(whiteSpan, startIndex, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        } else {
            builder.setSpan(redSpan, 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return builder;
    }

    /**
     * 创建指定数量的随机字符串
     *
     * @param numberFlag 是否是数字
     * @param length
     * @return
     */
    public static String createRandom(boolean numberFlag, int length) {
        String retStr = "";
        String strTable = numberFlag ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
        int len = strTable.length();
        boolean bDone = true;
        do {
            retStr = "";
            int count = 0;
            for (int i = 0; i < length; i++) {
                double dblR = Math.random() * len;
                int intR = (int) Math.floor(dblR);
                char c = strTable.charAt(intR);
                if (('0' <= c) && (c <= '9')) {
                    count++;
                }
                retStr += strTable.charAt(intR);
            }
            if (count >= 2) {
                bDone = false;
            }
        } while (bDone);

        return retStr;
    }

    /**
     * 将半角字符转换为全角字符
     *
     * @param input
     * @return
     */
    public static String ToSBC(String input) {
        char c[] = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == ' ') {
                c[i] = '\u3000';
            } else if (c[i] < '\177') {
                c[i] = (char) (c[i] + 65248);
            }
        }
        return new String(c);
    }

    public static boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && ((v instanceof EditText) || v instanceof RelativeLayout || v instanceof LinearLayout)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }

    /**
     * 判断字符串是否为null或者""
     */
    public static boolean isEmptyOrNull(String content) {
        if (content == null || content.equals("")) {
            return true;
        }
        return false;
    }

    /**
     * 涨幅的处理
     *
     * @param value
     * @return
     */
    public static boolean getValueWithoutMark(String value) {
        boolean flag = false;
        if (value.contains("%")) {
            flag = Float.valueOf(value.substring(0, value.length() - 1)) == 0f;
        } else {
            flag = Float.valueOf(value) == 0f;
        }
        return flag;
    }


    /**
     * 设置净值走势底部时间
     *
     * @param time
     * @param isYear
     * @return
     */
    public static String setHistoricalRealizableTime(String time, boolean isYear) {
        String timeStr = "";
        if (isYear) {
            timeStr = time.substring(0, 4) + "-" + time.substring(4, 6);
        } else {
            timeStr = time.substring(4, 6) + "-" + time.substring(6, time.length());
        }
        return timeStr;
    }

    /**
     * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
     * 定精度，以后的数字四舍五入。
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 表示表示需要精确到小数点以后几位。
     * @return 两个参数的商
     */
    public static double div(int v1, int v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(TypeConverUtils.convertToString(v1, "0"));
        BigDecimal b2 = new BigDecimal(TypeConverUtils.convertToString(v2, "0"));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 拨打电话
     *
     * @param context
     * @param phone
     */
    @SuppressLint("MissingPermission")
    public static void call(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    /**
     * 获取本地软件版本号
     */
    public static int getLocalVersion(Context ctx) {
        int localVersion = 0;
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

    public static String getWebTitle(String url) {
        try {
            //还是一样先从一个URL加载一个Document对象。
            Document doc = Jsoup.connect(url).get();
            Elements links = doc.select("head");
            Elements titlelinks = links.get(0).select("title");
            return titlelinks.get(0).text();
        } catch (Exception e) {
            return "";
        }
    }

}