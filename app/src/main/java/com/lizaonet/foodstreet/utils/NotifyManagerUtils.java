package com.lizaonet.foodstreet.utils;

import android.content.Intent;

import com.lizaonet.foodstreet.MainActivity;
import com.lizaonet.foodstreet.app.App;
import com.lizaonet.foodstreet.app.NotificationResutl;
import com.lizaonet.foodstreet.base.BaseWebViewAct;


/**
 * Created by caoxiuyun on 16/2/18.
 */
public class NotifyManagerUtils {

    private static NotifyManagerUtils instance;

    public static NotifyManagerUtils getInstance() {
        if (instance == null) {
            synchronized (NotifyManagerUtils.class) {
                if (instance == null) {
                    instance = new NotifyManagerUtils();
                }
            }
        }
        return instance;
    }

    /**
     * @param notificationResutlr
     */
    public void showNotification(NotificationResutl notificationResutl) {
        if (AccountUtil.getInstance().isLogin()) {
            Intent mainTabIntent = new Intent(App.getContext(), MainActivity.class);
            mainTabIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            App.getInstance().getApplicationContext().startActivity(mainTabIntent);
        } else {
            Intent intent = new Intent(App.getContext(), BaseWebViewAct.class);
//            intent.putExtra(BaseWebViewAct.URL, "login");
//            intent.putExtra(BaseWebViewAct.TITLE, "登录");
            App.getContext().startActivity(intent);
        }
    }
}
