package com.lizaonet.foodstreet.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {

    private static final int seconds_of_1day = 24 * 60 * 60;

    private static final int seconds_of_30days = seconds_of_1day * 30;

    /**
     * 时间戳转化为日期
     *
     * @return
     */
    public static String getTimeByMill(String tiem) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd  HH:mm");
        long time = Long.valueOf(tiem) * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String now_time = formatter.format(calendar.getTime());
        return now_time;
    }

    public static String getYMDByMill(String tiem) {
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        long time = Long.valueOf(tiem) * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String now_time = formatter.format(calendar.getTime());
        return now_time;
    }

    public static String getTimeForSendMsg(String tiem) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        long time = Long.valueOf(tiem) * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String now_time = formatter.format(calendar.getTime());
        return now_time;
    }

    public static String getYMDByMillFormar(String tiem) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        long time = Long.valueOf(tiem) * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String now_time = formatter.format(calendar.getTime());
        return now_time;
    }

    public static String getYMDByMillFormar(int tiem) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        long time = (long) tiem * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String now_time = formatter.format(calendar.getTime());
        return now_time;
    }

    public static String formatLongToTimeStr(Long l) {
        int hour = 0;
        int minute = 0;
        int second = 0;
        String formatHour = "";
        String formatMinute = "";
        String formatSecond = "";
        second = l.intValue();
        if (second > 60) {
            minute = second / 60;         //取整
            second = second % 60;         //取余
        }

        if (minute > 60) {
            hour = minute / 60;
            minute = minute % 60;
        }
        formatHour = hour < 10 ? "0" + hour : hour + "";
        formatMinute = minute < 10 ? "0" + minute : minute + "";
        formatSecond = second < 10 ? "0" + second : second + "";
        return formatHour + ":" + formatMinute + ":" + formatSecond;
    }

    public static String formatLongToResidueTimeStr(Long l) {
        int hour = 0;
        int minute = 0;
        int second = 0;
        String formatHour = "";
        String formatMinute = "";
        String formatSecond = "";
        second = l.intValue();
        if (second > 60) {
            minute = second / 60;         //取整
            second = second % 60;         //取余
        }

        if (minute > 60) {
            hour = minute / 60;
            minute = minute % 60;
        }
        formatHour = hour > 0 ? hour + "小时" : "";
        formatMinute = minute > 0 ? minute + "分" : "";
        formatSecond = second > 0 ? second + "秒" : "";
        return formatHour + formatMinute + formatSecond;
    }

    /**
     * @param time 毫秒值
     * @return 时:分:秒
     */
    public static String getHours(long time) {
        long second = time / 1000;
        long hour = second / 60 / 60;
        long minute = (second - hour * 60 * 60) / 60;
        long sec = (second - hour * 60 * 60) - minute * 60;

        String rHour = "";
        String rMin = "";
        String rSs = "";
        // 时
        if (hour < 10) {
            rHour = "0" + hour;
        } else {
            rHour = hour + "";
        }
        // 分
        if (minute < 10) {
            rMin = "0" + minute;
        } else {
            rMin = minute + "";
        }
        // 秒
        if (sec < 10) {
            rSs = "0" + sec;
        } else {
            rSs = sec + "";
        }

        // return hour + "小时" + minute + "分钟" + sec + "秒";
        return rHour + "时" + rMin + "分" + rSs + "秒";

    }

    /**
     * 获取当前日期是周几
     *
     * @param dt
     * @return 当前日期是周几
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * 获取未来 第 past 天的日期
     *
     * @param past
     * @return
     */
    public static String getFetureDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        String result = format.format(today);
        return result;
    }
}

