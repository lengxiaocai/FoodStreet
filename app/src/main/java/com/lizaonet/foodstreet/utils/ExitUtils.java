package com.lizaonet.foodstreet.utils;

import android.app.Activity;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 退出时toast询问是否退出的工具类
 */
public class ExitUtils {

    private static ExitUtils exitUtils = null;
    // 计时器
    private MyCount mc;
    private boolean isExit;
    private boolean hasTask;

    private ExitUtils() {
    }

    public static ExitUtils getInstance() {
        if (exitUtils == null) {
            exitUtils = new ExitUtils();
        }
        return exitUtils;
    }

    public boolean isExit(Activity activity) {
        if (isExit == false) {
            isExit = true;
            Toast.makeText(activity, "再按一次退出", Toast.LENGTH_SHORT).show();
            if (!hasTask) {
                hasTask = true;
                mc = new MyCount(2000, 1000);
                mc.start();
            }
        } else {
            return true;
        }
        return false;
    }

    /* 定义一个倒计时的内部类 */
    class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            isExit = false;
            hasTask = false;
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }
    }

    public static void setPricePoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }

                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });

    }
}
