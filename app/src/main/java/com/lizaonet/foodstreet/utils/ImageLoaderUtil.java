package com.lizaonet.foodstreet.utils;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.app.App;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;


public class ImageLoaderUtil extends ImageLoader {
    private static ImageLoaderUtil instance;

    public static ImageLoaderUtil getImageLoader() {
        if (instance == null) {
            synchronized (ImageLoaderUtil.class) {
                if (instance == null) {
                    instance = new ImageLoaderUtil();
                    initImageLoader(instance);
                }
            }
        }
        return instance;
    }

    private static void initImageLoader(ImageLoader loader) {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)// 缓存到内存
                .cacheOnDisk(true) // 缓存到SD卡
                .showImageOnLoading(R.mipmap.local_image)
                .showImageForEmptyUri(R.mipmap.local_image)
                .showImageOnFail(R.mipmap.local_image)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .considerExifParams(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(App.getContext())
                .defaultDisplayImageOptions(defaultOptions)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .diskCacheExtraOptions(800, 1280, null)
                .build();
        loader.init(config);
    }


    /**
     * @param isNotJudge 是否需要判断用户设置的是否加载图片，如果为true,则一定会去加载图片，如果为false，则会去判断是否加载图片
     */
    public void displayImage(String uri, ImageView imageAware, boolean isNotJudge) {
        if (isNotJudge) {
            super.displayImage(uri, imageAware);
        } else {
            displayImage(uri, imageAware);
        }
    }

    /**
     * 加载图片失败使用默认图
     *
     * @param imageView
     * @param url
     */
    public void getImage(final String url, final ImageView imageView, final int defImageResId) {
        DisplayImageOptions optionsT = new DisplayImageOptions.Builder()
                .showImageOnFail(defImageResId)
                .showImageForEmptyUri(defImageResId)
                .showImageOnFail(defImageResId)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        displayImage(url, imageView, optionsT);
    }

    public void setRoundImage(final String url, ImageView imageView, int round) {
        DisplayImageOptions optionsT = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(round))
                .build();
        displayImage(url, imageView, optionsT);
    }

    public Bitmap getBitMapFromUrl(String url) {
        return loadImageSync(url);
    }

    public void displayClearPic(String uri, ImageView imageView, int Img) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .considerExifParams(true)
                .cacheOnDisk(true)
                .showImageOnLoading(Img)
                .showImageForEmptyUri(Img)
                .showImageOnFail(Img)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();
        displayImage(uri, imageView, options);
    }


    /**
     * 获取缓存路径
     *
     * @param url
     * @return
     */
    public String getCacheBitmap(String url) {
        return instance.getDiskCache().get(url).getPath();
    }

    public void loadImage(String url, ImageView imageView, ImageLoadingListener listener) {
        displayImage(url, imageView, listener);
    }
}
