package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.socks.library.KLog;

public class RegisterAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("注册");
    }

    @Override
    protected String htmlFileName() {
        return "register";
    }

    @Override
    protected void changeAct(String paramString) {
        KLog.d("----------------", paramString);
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if ("login".equals(baseResponse.getTo())) {
            Intent registerIntent = new Intent(RegisterAct.this, LoginAct.class);
            startActivity(registerIntent);
            finish();
        }
    }

    @Override
    protected String callJs(String result) {
        return "";
    }
}
