package com.lizaonet.foodstreet.module.self.act;

import android.net.Uri;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseActivity;
import com.lizaonet.foodstreet.module.self.datatool.SelfDataTool;
import com.lizaonet.foodstreet.module.self.datatool.UserInfoResult;
import com.lizaonet.foodstreet.net.VolleyCallBack;

import butterknife.BindView;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;

public class ConversationActivity extends BaseActivity {

    private boolean isinBlack = false;
    @BindView(R.id.rl_left)
    RelativeLayout rl_left;
    @BindView(R.id.ll_right)
    LinearLayout ll_right;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_title)
    TextView tv_title;

    private UserInfo userInfo;

    @Override
    protected void initViews() {
        mToolbar.setVisibility(View.GONE);
        String sName = getIntent().getData().getQueryParameter("title");
        tv_name.setText(sName);
        final String uid = getIntent().getData().getQueryParameter("targetId");
        getChatUserInfo();
        RongIM.getInstance().getBlacklistStatus(uid, new RongIMClient.ResultCallback<RongIMClient.BlacklistStatus>() {
            @Override
            public void onSuccess(RongIMClient.BlacklistStatus blacklistStatus) {
                if (blacklistStatus == RongIMClient.BlacklistStatus.IN_BLACK_LIST) {
                    tv_title.setText("移出黑名单");
                    isinBlack = true;
                } else {
                    tv_title.setText("拉黑");
                    isinBlack = false;
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {

            }
        });

        ll_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isinBlack) {
                    RongIM.getInstance().removeFromBlacklist(uid, new RongIMClient.OperationCallback() {
                        @Override
                        public void onSuccess() {
                            tv_title.setText("拉黑");
                            isinBlack = false;
                        }

                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });
                } else {
                    RongIM.getInstance().addToBlacklist(uid, new RongIMClient.OperationCallback() {
                        @Override
                        public void onSuccess() {
                            tv_title.setText("移出黑名单");
                            isinBlack = true;
                        }

                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });

                }
            }
        });

        rl_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_conversation;
    }

    @Override
    public void onBackPressed() {
        ConversationFragment fragment = (ConversationFragment) getSupportFragmentManager().findFragmentById(R.id.conversation);
        if (!fragment.onBackPressed()) {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }


    private void getChatUserInfo() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                RongIM.setUserInfoProvider(new RongIM.UserInfoProvider() {
                    @Override
                    public UserInfo getUserInfo(String userId) {
                        return findUserById(userId);//根据 userId 去你的用户系统里查询对应的用户信息返回给融云 SDK。
                    }
                }, true);

            }
        }, 500);
    }

    private UserInfo findUserById(final String userId) {
        SelfDataTool.getInstance().getUserInfo(false, ConversationActivity.this, userId, new VolleyCallBack<UserInfoResult>() {
            @Override
            public void loadSucceed(UserInfoResult result) {
                if (result.isSucc()) {
                    userInfo = new UserInfo(userId, result.getUsername(), Uri.parse(result.getHead_img()));
                    RongIM.getInstance().refreshUserInfoCache(userInfo);
                }
            }

            @Override
            public void loadFailed(VolleyError error) {

            }
        });
        return userInfo;
    }
}
