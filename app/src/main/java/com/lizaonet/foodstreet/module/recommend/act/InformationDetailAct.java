package com.lizaonet.foodstreet.module.recommend.act;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.FragmentActivity;
import android.text.ClipboardManager;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.SendCommentAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;

import java.lang.ref.WeakReference;

public class InformationDetailAct extends BaseWebViewAct {


    //友盟分享相关
    private UMShareListener mShareListener;
    private ShareAction mShareAction;
    public static String ID = "id";
    private String id = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("资讯详情");
        id = getIntent().getStringExtra(ID);
    }

    @Override
    protected String htmlFileName() {
        return "informationdetail";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "share":
                    String shareUrl = baseResponse.getShareurl() + id + ".html";
                    initShare(shareUrl, baseResponse.getContent(), baseResponse.getContent(), baseResponse.getImg());
                    break;
                case "comment":
                    Intent intent = new Intent(InformationDetailAct.this, SendCommentAct.class);
                    intent.putExtra(SendCommentAct.ID, baseResponse.getId());
                    intent.putExtra(SendCommentAct.TYPE, baseResponse.getType());
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'informid': '" + id + "'})";
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mShareAction.close();
    }


    private void initShare(final String shareUrl, final String title, final String desc, final String img) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mShareListener = new CustomShareListener(InformationDetailAct.this);
                mShareAction = new ShareAction(InformationDetailAct.this).setDisplayList(
                        SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.WEIXIN_FAVORITE,
                        SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE)
                        .addButton("umeng_sharebutton_custom", "umeng_sharebutton_custom", "umeng_socialize_copyurl", "umeng_socialize_copyurl")
                        .setShareboardclickCallback(new ShareBoardlistener() {
                            @Override
                            public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                                if (snsPlatform.mKeyword.equals("umeng_sharebutton_custom")) {
                                    ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                    // 将文本内容放到系统剪贴板里。
                                    if (cm != null) {
                                        cm.setText(shareUrl);
                                        Tips.instance.tipShort("复制成功");
                                    }
                                } else {
                                    UMWeb web = new UMWeb(shareUrl);
                                    web.setTitle(title);
                                    web.setDescription(desc);
                                    if (img.isEmpty()) {
                                        web.setThumb(new UMImage(InformationDetailAct.this, R.mipmap.ic_launcher));
                                    } else {
                                        web.setThumb(new UMImage(InformationDetailAct.this, img));
                                    }
                                    new ShareAction(InformationDetailAct.this).withMedia(web)
                                            .setPlatform(share_media)
                                            .setCallback(mShareListener)
                                            .share();
                                }
                            }
                        });
                mShareAction.open();
            }
        });

    }


    private class CustomShareListener implements UMShareListener {

        private WeakReference mActivity;

        private CustomShareListener(FragmentActivity activity) {
            mActivity = new WeakReference(activity);
        }

        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            if (platform.name().equals("WEIXIN_FAVORITE")) {
                Tips.instance.tipShort(platform + " 收藏成功啦");
            } else {
                if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS
                        && platform != SHARE_MEDIA.EMAIL
                        && platform != SHARE_MEDIA.FLICKR
                        && platform != SHARE_MEDIA.FOURSQUARE
                        && platform != SHARE_MEDIA.TUMBLR
                        && platform != SHARE_MEDIA.POCKET
                        && platform != SHARE_MEDIA.PINTEREST
                        && platform != SHARE_MEDIA.INSTAGRAM
                        && platform != SHARE_MEDIA.GOOGLEPLUS
                        && platform != SHARE_MEDIA.YNOTE
                        && platform != SHARE_MEDIA.EVERNOTE) {
                    Tips.instance.tipShort(" 分享成功");
                }
            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS
                    && platform != SHARE_MEDIA.EMAIL
                    && platform != SHARE_MEDIA.FLICKR
                    && platform != SHARE_MEDIA.FOURSQUARE
                    && platform != SHARE_MEDIA.TUMBLR
                    && platform != SHARE_MEDIA.POCKET
                    && platform != SHARE_MEDIA.PINTEREST

                    && platform != SHARE_MEDIA.INSTAGRAM
                    && platform != SHARE_MEDIA.GOOGLEPLUS
                    && platform != SHARE_MEDIA.YNOTE
                    && platform != SHARE_MEDIA.EVERNOTE) {
                Tips.instance.tipShort(" 分享失败");
                if (t != null) {
                    com.umeng.socialize.utils.Log.d("throw", "throw:" + t.getMessage());
                }
            }

        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Tips.instance.tipShort(" 取消分享了");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.reload();
    }
}
