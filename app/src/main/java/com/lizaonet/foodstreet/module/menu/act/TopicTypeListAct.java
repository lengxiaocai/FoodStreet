package com.lizaonet.foodstreet.module.menu.act;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.model.MenuCidEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

import io.rong.eventbus.EventBus;

public class TopicTypeListAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("话题板块");
    }

    @Override
    protected String htmlFileName() {
        return "topictypelist";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "topictype":
                    String cid = baseResponse.getId();
                    EventBus.getDefault().post(new MenuCidEvent(cid));
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}



