package com.lizaonet.foodstreet.module.self.act;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.Toast;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.FileUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MyInfoAct extends BaseWebViewAct {
    private static final int REQUEST_CODE_CHOOSE = 24;
    private static final int REQUEST_PHOTO_CHOOSE = 25;
    private int MAXSIZE = 1;

    private List<String> fileList = new ArrayList<>();
    private List<String> filePhotoList = new ArrayList<>();

    String funName = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("基本信息");
        showTitleRightBtn("保存", 0);
        public_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebviewUtils.getInstance().callJs("modifyinfo()", mWebView);
            }
        });
    }

    @Override
    protected String htmlFileName() {
        return "baseinfo";
    }

    @Override
    protected void changeAct(String paramString) {
        final BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if ("uploadHeader".equals(baseResponse.getTo())) {
            selectImg();
        }
        if ("uploadimg".equals(baseResponse.getTo())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    uploadPhoto();
                    funName = baseResponse.getFuncname();
                }
            });
        }
        if ("selectcity".equals(baseResponse.getTo())) {
            Intent intent = new Intent(MyInfoAct.this, SelectCityAct.class);
            startActivity(intent);
        }
        if ("bindphone".equals(baseResponse.getTo())) {
            Intent intent = new Intent(MyInfoAct.this, BindPhoneAct.class);
            startActivity(intent);
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }

    private void selectImg() {
        RxPermissions rxPermissions = new RxPermissions(MyInfoAct.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            Matisse.from(MyInfoAct.this)
                                    .choose(MimeType.ofImage(), false)
                                    .countable(true)
                                    .capture(true)
                                    .captureStrategy(new CaptureStrategy(true, "com.lizaonet.foodstreet.fileprovider"))
                                    .maxSelectable(MAXSIZE)
                                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.imgPicker))
                                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    .thumbnailScale(0.85f)
                                    .imageEngine(new GlideEngine())
                                    .forResult(REQUEST_CODE_CHOOSE);
                        } else {
                            Toast.makeText(MyInfoAct.this, R.string.permission_request_denied, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    private void uploadPhoto() {
        RxPermissions rxPermissions = new RxPermissions(MyInfoAct.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            Matisse.from(MyInfoAct.this)
                                    .choose(MimeType.ofImage(), false)
                                    .countable(true)
                                    .capture(true)
                                    .captureStrategy(new CaptureStrategy(true, "com.lizaonet.foodstreet.fileprovider"))
                                    .maxSelectable(MAXSIZE)
                                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.imgPicker))
                                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    .thumbnailScale(0.85f)
                                    .imageEngine(new GlideEngine())
                                    .forResult(REQUEST_PHOTO_CHOOSE);
                        } else {
                            Toast.makeText(MyInfoAct.this, R.string.permission_request_denied, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            fileList.clear();
            fileList.addAll(Matisse.obtainPathResult(data));
            final String fileToBase64 = FileUtil.bitmapToString(fileList.get(0));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebviewUtils.getInstance().callJs("doUploadHeader('" + fileList.get(0) + "','" + fileToBase64 + "')", mWebView);
                }
            });
        }

        if (requestCode == REQUEST_PHOTO_CHOOSE && resultCode == RESULT_OK) {
            filePhotoList.clear();
            filePhotoList.addAll(Matisse.obtainPathResult(data));
            final String fileToBase64 = FileUtil.bitmapToString(filePhotoList.get(0));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebviewUtils.getInstance().callJs(funName + "('" + filePhotoList.get(0) + "','" + fileToBase64 + "')", mWebView);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        WebviewUtils.getInstance().callJs("setphone" + "('" + PreferenceHelper.getString(GlobalConstants.PHONE, "") + "')", mWebView);
    }
}
