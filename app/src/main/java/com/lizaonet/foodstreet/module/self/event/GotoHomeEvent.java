package com.lizaonet.foodstreet.module.self.event;

/**
 * Created by caoxiuyun on 2017/8/24.
 */

public class GotoHomeEvent {
    private boolean gotoHome;

    public GotoHomeEvent(boolean gotoHome) {
        this.gotoHome = gotoHome;
    }

    public boolean isGotoHome() {

        return gotoHome;
    }

    public void setGotoHome(boolean gotoHome) {
        this.gotoHome = gotoHome;
    }
}
