package com.lizaonet.foodstreet.module.self.act;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class MyMsgDetailAct extends BaseWebViewAct {

    public static String ID = "id";
    private String id = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("系统消息");
        id = getIntent().getStringExtra(ID);
    }

    @Override
    protected String htmlFileName() {
        return "msgdetail";
    }

    @Override
    protected void changeAct(String paramString) {
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'newsid': '" + id + "'})";
    }
}
