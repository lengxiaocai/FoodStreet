package com.lizaonet.foodstreet.module.menu.model;

public class SendFoodStepEvent {
    private String data;

    public SendFoodStepEvent(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
