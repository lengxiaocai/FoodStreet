package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.BookDetailAct;
import com.lizaonet.foodstreet.module.recommend.act.InformationDetailAct;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class NearViewAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("最近浏览");
    }

    @Override
    protected String htmlFileName() {
        return "nearview";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "bookdetail":
                    Intent menudetailIntent = new Intent(NearViewAct.this, BookDetailAct.class);
                    menudetailIntent.putExtra(BookDetailAct.ID, baseResponse.getId());
                    startActivity(menudetailIntent);
                    break;
                case "topicdetail":
                    Intent intent = new Intent(NearViewAct.this, TopicDetailAct.class);
                    intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                    startActivity(intent);
                    break;
                case "informationdetail":
                    Intent informationdetailIntent = new Intent(NearViewAct.this, InformationDetailAct.class);
                    informationdetailIntent.putExtra(InformationDetailAct.ID, baseResponse.getId());
                    startActivity(informationdetailIntent);
                    break;
                default:
                    break;
            }
        }

    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
