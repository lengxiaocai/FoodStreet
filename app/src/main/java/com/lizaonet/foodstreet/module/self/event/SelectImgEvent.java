package com.lizaonet.foodstreet.module.self.event;

import java.util.ArrayList;
import java.util.List;

public class SelectImgEvent {
    private List<String> fileList;

    public SelectImgEvent(List<String> fileList) {
        this.fileList = fileList;
    }

    public List<String> getFileList() {
        if (fileList == null) {
            return new ArrayList<>();
        }
        return fileList;
    }
}
