package com.lizaonet.foodstreet.module.self.act;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseActivity;
import com.lizaonet.foodstreet.module.self.PhotoDetailFragment;
import com.lizaonet.foodstreet.views.HackyViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import me.relex.circleindicator.CircleIndicator;


/**
 * Created by caoxiuyun on 16/7/5.
 */

public class PhotoBrowsekAct extends BaseActivity {

    private ArrayList<String> urlList = new ArrayList<>();
    public static String PICURLLIST = "picurllist";
    public static String ISSHOWINDICATOR = "showindicator";
    public static String SELECTPOSITION = "selectposition";
    public static String ISSHOWDEL = "isshowdel";
    public static String WHERE = "where";
    private boolean isShowInDicator = true;
    private int selectPosition = 0;
    private boolean isShowDel = false;
    private ImagePagerAdapter adapter;
    @BindView(R.id.pager)
    HackyViewPager pager;

    @BindView(R.id.indicator)
    CircleIndicator indicator;

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_photo_browse;
    }

    @Override
    protected void initViews() {
        setSwipeBackEnable(true);
        urlList = getIntent().getStringArrayListExtra(PICURLLIST);
        if (urlList.size() == 0) {
            urlList = getIntent().getExtras().getStringArrayList(PICURLLIST);
        }
        selectPosition = getIntent().getIntExtra(SELECTPOSITION, 0);
        isShowInDicator = getIntent().getBooleanExtra(ISSHOWINDICATOR, true);
        isShowDel = getIntent().getBooleanExtra(ISSHOWDEL, false);
        adapter = new ImagePagerAdapter(getSupportFragmentManager(), urlList);
        pager.setAdapter(adapter);
        mToolbar.setVisibility(View.GONE);
        setTitleMiddleText((selectPosition + 1) + "/" + urlList.size());
        if (urlList.size() == 1 || !isShowInDicator) {
            indicator.setVisibility(View.GONE);
        } else {
            indicator.setViewPager(pager);
        }
        if (urlList.size() > 1 && isShowDel) {
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    setTitleMiddleText((selectPosition + 1) + "/" + urlList.size());
                }

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
        pager.setCurrentItem(selectPosition);
    }

    private class ImagePagerAdapter extends FragmentStatePagerAdapter {

        ArrayList<String> fileList;

        ImagePagerAdapter(FragmentManager fm, ArrayList<String> fileList) {
            super(fm);
            this.fileList = fileList;
        }

        @Override
        public int getCount() {
            return fileList == null ? 0 : fileList.size();
        }

        @Override
        public Fragment getItem(int position) {
            String url = fileList.get(position);
            return PhotoDetailFragment.newInstance(url);
        }

    }
}
