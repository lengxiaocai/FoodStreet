package com.lizaonet.foodstreet.module.menu.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.net.JSONUtils;

public class BookListAct extends BaseWebViewAct {
    public static String ID = "id";
    private String id = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("菜谱列表");
        id = getIntent().getStringExtra(ID);
    }

    @Override
    protected String htmlFileName() {
        return "booklist";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, com.lizaonet.foodstreet.base.BaseResponse.class);
        if (baseResponse.getTo() != null) switch (baseResponse.getTo()) {
            case "bookdetail":
                Intent intent = new Intent(BookListAct.this, BookDetailAct.class);
                intent.putExtra(BookListAct.ID, baseResponse.getId());
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected String callJs(String result) {
        return "setParams({'c_id': '" + id + "'})";
    }
}
