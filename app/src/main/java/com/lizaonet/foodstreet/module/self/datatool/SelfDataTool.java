package com.lizaonet.foodstreet.module.self.datatool;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.lizaonet.foodstreet.config.ConfigServerInterface;
import com.lizaonet.foodstreet.net.VolleyCallBack;
import com.lizaonet.foodstreet.net.VolleyUtil;

import java.util.Map;

/**
 * Created by caoxiuyun on 16/4/6.
 */
public class SelfDataTool {
    private static SelfDataTool instance;

    private SelfDataTool() {
    }

    public static SelfDataTool getInstance() {
        if (instance == null) {
            synchronized (SelfDataTool.class) {
                if (instance == null) {
                    instance = new SelfDataTool();
                }
            }
        }
        return instance;
    }

    /**
     * 获取信息
     *
     * @param needDialog
     * @param context
     * @param userid
     * @param callBack
     */
    public void getUserInfo(boolean needDialog, Context context, String userid, VolleyCallBack<UserInfoResult> callBack) {
        Map<String, String> params = new ArrayMap<String, String>();
        params.put("ru_uid", userid);
        VolleyUtil.getInstance().post(needDialog, context, ConfigServerInterface.USERINFO, params, UserInfoResult.class, callBack);
    }

}
