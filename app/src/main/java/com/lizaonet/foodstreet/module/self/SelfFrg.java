package com.lizaonet.foodstreet.module.self;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseFragment;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.FeedBackAct;
import com.lizaonet.foodstreet.module.self.act.LoginAct;
import com.lizaonet.foodstreet.module.self.act.MyAttentionAct;
import com.lizaonet.foodstreet.module.self.act.MyBookAct;
import com.lizaonet.foodstreet.module.self.act.MyCollectAct;
import com.lizaonet.foodstreet.module.self.act.MyExchangeAct;
import com.lizaonet.foodstreet.module.self.act.MyInfoAct;
import com.lizaonet.foodstreet.module.self.act.MyIntegralAct;
import com.lizaonet.foodstreet.module.self.act.MyMsgAct;
import com.lizaonet.foodstreet.module.self.act.MyTopicAct;
import com.lizaonet.foodstreet.module.self.act.NearViewAct;
import com.lizaonet.foodstreet.module.self.event.GotoHomeEvent;
import com.lizaonet.foodstreet.module.self.event.SelectImgEvent;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.FileUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.StringUtils;
import com.lizaonet.foodstreet.utils.Tips;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.rong.eventbus.EventBus;

public class SelfFrg extends BaseFragment {

    private static final int REQUEST_CODE_CHOOSE = 23;
    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    private X5WebView mWebView;
    private List<String> fileList = new ArrayList<>();

    private int MAXSIZE = 1;

    //友盟分享相关
    private UMShareListener mShareListener;
    private ShareAction mShareAction;

    public static SelfFrg getInstance() {
        return new SelfFrg();
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_base_webview;
    }

    @Override
    protected void initViews() {
        setTitleMiddleText("我的");
        initWebView();
        showTitleRightBtn();
        mWebView.setWebChromeClient(new WebChromeClient() {

            IX5WebChromeClient.CustomViewCallback callback;

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                Log.d("111111111", arg3 + "");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);

            }

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {

                callback = customViewCallback;
            }
        });

        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(getActivity());
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        setKuaYu();
        mWebView.loadUrl("file:///android_asset/appweb/html/mine.html");
    }

    private void initWebView() {
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    private void setKuaYu() {

        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void selectImg() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            Matisse.from(getActivity())
                                    .choose(MimeType.ofImage(), false)
                                    .countable(true)
                                    .capture(true)
                                    .captureStrategy(new CaptureStrategy(true, "com.lizaonet.foodstreet.fileprovider"))
                                    .maxSelectable(MAXSIZE)
                                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.imgPicker))
                                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    .thumbnailScale(0.85f)
                                    .imageEngine(new GlideEngine())
                                    .forResult(REQUEST_CODE_CHOOSE);
                        } else {
                            Toast.makeText(getActivity(), R.string.permission_request_denied, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void onEventMainThread(SelectImgEvent event) {
        if (event.getFileList().size() > 0) {
            fileList.clear();
            fileList.addAll(event.getFileList());
            String fileToBase64 = FileUtil.bitmapToString(fileList.get(0));
            callJs("doUploadHeader('" + fileList.get(0) + "','" + fileToBase64 + "')");
        }
    }

    private void callJs(String jsString) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            mWebView.loadUrl("javascript:" + jsString);
        } else {
            mWebView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    //此处为 js 返回的结果

                    Log.d("11111111", value);
                }
            });
        }
    }

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public String GetChannel() {
            return "aaaa";
        }

        @JavascriptInterface
        public void changeActivity() {

        }


        @JavascriptInterface
        public void initParams(String result) {
            final String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
            final String versionCode = StringUtils.getLocalVersionName(getActivity());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callJs("setParams({'uid': '" + uid + "', 'version': '" + versionCode + "'})");
                }
            });

        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if (baseResponse.getTo() != null) {
                switch (baseResponse.getTo()) {
                    case "baseinfo":
                        Intent myinfoIntent = new Intent(getActivity(), MyInfoAct.class);
                        startActivity(myinfoIntent);
                        break;
                    case "mybook":
                        Intent mybookIntent = new Intent(getActivity(), MyBookAct.class);
                        startActivity(mybookIntent);
                        break;
                    case "mytopic":
                        Intent mytopicIntent = new Intent(getActivity(), MyTopicAct.class);
                        startActivity(mytopicIntent);
                        break;
                    case "myexchange":
                        Intent myexchangeIntent = new Intent(getActivity(), MyExchangeAct.class);
                        startActivity(myexchangeIntent);
                        break;
                    case "feedback":
                        Intent feedbackIntent = new Intent(getActivity(), FeedBackAct.class);
                        startActivity(feedbackIntent);
                        break;
                    case "mycollect":
                        Intent mycollectIntent = new Intent(getActivity(), MyCollectAct.class);
                        startActivity(mycollectIntent);
                        break;
                    case "mymessage":
                        Intent mymsgIntent = new Intent(getActivity(), MyMsgAct.class);
                        startActivity(mymsgIntent);
                        break;
                    case "savedata":
                        PreferenceHelper.putString(GlobalConstants.USERNAME, baseResponse.getNickname());
                        PreferenceHelper.putString(GlobalConstants.HEAD, baseResponse.getHead());
                        break;
                    case "myattention":
                        Intent myattentionIntent = new Intent(getActivity(), MyAttentionAct.class);
                        startActivity(myattentionIntent);
                        break;
                    case "nearview":
                        Intent nearviewIntent = new Intent(getActivity(), NearViewAct.class);
                        startActivity(nearviewIntent);
                        break;
                    case "nologin":
                        Intent loginIntent = new Intent(getActivity(), LoginAct.class);
                        startActivity(loginIntent);
                        break;
                    case "integral":
                        Intent integralIntent = new Intent(getActivity(), MyIntegralAct.class);
                        startActivity(integralIntent);
                        break;
                    case "exit":
                        PreferenceHelper.putString(GlobalConstants.UID, "");
                        PreferenceHelper.putString(GlobalConstants.PHONE, "");
                        PreferenceHelper.putString(GlobalConstants.TOKEN, "");
                        PreferenceHelper.putString(GlobalConstants.USERNAME, "");
                        PreferenceHelper.putString(GlobalConstants.HEAD, "");
                        PreferenceHelper.putString(GlobalConstants.RONGIMUID, "");
                        EventBus.getDefault().post(new GotoHomeEvent(true));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void onEventMainThread(LoginEvent event) {
        if (!event.getUpdate().isEmpty()) {
            mWebView.reload();
        }
    }

    private void initShare(final String shareUrl, final String title, final String desc, final String img) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mShareListener = new CustomShareListener(getActivity());
                mShareAction = new ShareAction(getActivity()).setDisplayList(
                        SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.WEIXIN_FAVORITE,
                        SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE)
                        .setShareboardclickCallback(new ShareBoardlistener() {
                            @Override
                            public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                                UMWeb web = new UMWeb(shareUrl);
                                web.setTitle(title);
                                web.setDescription(desc);
                                web.setThumb(new UMImage(getActivity(), img));
                                new ShareAction(getActivity()).withMedia(web)
                                        .setPlatform(share_media)
                                        .setCallback(mShareListener)
                                        .share();
                            }
                        });
                mShareAction.open();
            }
        });

    }

    private class CustomShareListener implements UMShareListener {

        private WeakReference mActivity;

        private CustomShareListener(FragmentActivity activity) {
            mActivity = new WeakReference(activity);
        }

        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            if (platform.name().equals("WEIXIN_FAVORITE")) {
                Tips.instance.tipShort(platform + " 收藏成功啦");
            } else {
                if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS
                        && platform != SHARE_MEDIA.EMAIL
                        && platform != SHARE_MEDIA.FLICKR
                        && platform != SHARE_MEDIA.FOURSQUARE
                        && platform != SHARE_MEDIA.TUMBLR
                        && platform != SHARE_MEDIA.POCKET
                        && platform != SHARE_MEDIA.PINTEREST
                        && platform != SHARE_MEDIA.INSTAGRAM
                        && platform != SHARE_MEDIA.GOOGLEPLUS
                        && platform != SHARE_MEDIA.YNOTE
                        && platform != SHARE_MEDIA.EVERNOTE) {
                    Tips.instance.tipShort(" 分享成功");
                }

            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            if (platform != SHARE_MEDIA.MORE && platform != SHARE_MEDIA.SMS
                    && platform != SHARE_MEDIA.EMAIL
                    && platform != SHARE_MEDIA.FLICKR
                    && platform != SHARE_MEDIA.FOURSQUARE
                    && platform != SHARE_MEDIA.TUMBLR
                    && platform != SHARE_MEDIA.POCKET
                    && platform != SHARE_MEDIA.PINTEREST

                    && platform != SHARE_MEDIA.INSTAGRAM
                    && platform != SHARE_MEDIA.GOOGLEPLUS
                    && platform != SHARE_MEDIA.YNOTE
                    && platform != SHARE_MEDIA.EVERNOTE) {
                Tips.instance.tipShort(" 分享失败");
                if (t != null) {
                    com.umeng.socialize.utils.Log.d("throw", "throw:" + t.getMessage());
                }
            }

        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Tips.instance.tipShort(" 取消分享了");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mShareAction.close();
    }


    public void onEventMainThread(GotoHomeEvent event) {
        if (event.isGotoHome()) {
           mWebView.reload();
        }
    }
}
