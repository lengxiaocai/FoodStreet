package com.lizaonet.foodstreet.module.topic;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseFragment;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.CommonWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.SearchAct;
import com.lizaonet.foodstreet.module.menu.act.TopicTypeListAct;
import com.lizaonet.foodstreet.module.menu.model.MenuCidEvent;
import com.lizaonet.foodstreet.module.self.act.LoginAct;
import com.lizaonet.foodstreet.module.self.act.ProfileAct;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.module.topic.act.SendTopicAct;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.AccountUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;

public class TopicFrg extends BaseFragment {

    @BindView(R.id.iv_search)
    ImageView iv_search;
    @BindView(R.id.ll_1)
    LinearLayout ll_1;
    @BindView(R.id.ll_2)
    LinearLayout ll_2;
    @BindView(R.id.ll_3)
    LinearLayout ll_3;
    @BindView(R.id.ll_4)
    LinearLayout ll_4;
    @BindView(R.id.view_1)
    View view_1;
    @BindView(R.id.view_2)
    View view_2;
    @BindView(R.id.view_3)
    View view_3;
    @BindView(R.id.view_4)
    View view_4;
    @BindView(R.id.ll_send_topic)
    LinearLayout ll_send_topic;
    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    private X5WebView mWebView;

    public static TopicFrg getInstance() {
        return new TopicFrg();
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.frg_topic;
    }

    @Override
    protected void initViews() {
        toolbar.setVisibility(View.GONE);
        initWebView();
        showTitleRightBtn();
        mWebView.setWebChromeClient(new WebChromeClient() {

            IX5WebChromeClient.CustomViewCallback callback;

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                Log.d("111111111", arg3 + "");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);

            }

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {

                callback = customViewCallback;
            }
        });

        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(getActivity());
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        setKuaYu();
        mWebView.loadUrl("file:///android_asset/appweb/html/topic.html");
        ll_send_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccountUtil.getInstance().isLogin()) {
                    Intent intent = new Intent(getActivity(), SendTopicAct.class);
                    startActivity(intent);
                } else {
                    Tips.instance.tipShort("请先登录");
                    Intent intent = new Intent(getActivity(), LoginAct.class);
                    startActivity(intent);
                }
            }
        });
        ll_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changestatus(view_1);
                WebviewUtils.getInstance().callJs("changetyep('" + "1" + "')", mWebView);
            }
        });
        ll_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changestatus(view_2);
                WebviewUtils.getInstance().callJs("changetyep('" + "" + "')", mWebView);
            }
        });
        ll_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changestatus(view_3);
                WebviewUtils.getInstance().callJs("changetyep('" + "2" + "')", mWebView);
            }
        });
        ll_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changestatus(view_4);
                WebviewUtils.getInstance().callJs("changetyep('" + "3" + "')", mWebView);
            }
        });
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAct.class);
                intent.putExtra(SearchAct.TYPE, "2");
                intent.putExtra(SearchAct.TITLE, "话题");
                startActivity(intent);
            }
        });
    }

    private void initWebView() {
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    private void setKuaYu() {

        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void callJs(String jsString) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            mWebView.loadUrl("javascript:" + jsString);
        } else {
            mWebView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    //此处为 js 返回的结果

                    Log.d("11111111", value);
                }
            });
        }
    }

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public String GetChannel() {
            return "aaaa";
        }

        @JavascriptInterface
        public void changeActivity() {

        }


        @JavascriptInterface
        public void initParams(String result) {
            final String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callJs("setParams({'uid': '" + uid + "'})");
                }
            });

        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if (baseResponse.getTo() != null) {
                switch (baseResponse.getTo()) {
                    case "topicdetail":
                        Intent intent = new Intent(getActivity(), TopicDetailAct.class);
                        intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                        startActivity(intent);
                        break;
                    case "notlogin":
                        Tips.instance.tipShort("未登录");
                        break;
                    case "more":
                        Intent topictypelistIntent = new Intent(getActivity(), TopicTypeListAct.class);
                        startActivity(topictypelistIntent);
                        break;
                    case "bannerClick":
                        Intent bannerIntent = new Intent(getActivity(), CommonWebViewAct.class);
                        bannerIntent.putExtra(CommonWebViewAct.URL, baseResponse.getLink_url());
                        startActivity(bannerIntent);
                        break;
                    case "profile":
                        if (AccountUtil.getInstance().isLogin()) {
                            Intent profileIntent = new Intent(getActivity(), ProfileAct.class);
                            profileIntent.putExtra(ProfileAct.FUID, baseResponse.getFuid());
                            startActivity(profileIntent);
                        } else {
                            Tips.instance.tipShort("请先登录");
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void onEventMainThread(LoginEvent event) {
        if (!event.getUpdate().isEmpty()) {
            mWebView.reload();
            changestatus(view_1);
        }
    }

    private void changestatus(View view) {
        view_1.setVisibility(View.INVISIBLE);
        view_2.setVisibility(View.INVISIBLE);
        view_3.setVisibility(View.INVISIBLE);
        view_4.setVisibility(View.INVISIBLE);
        view.setVisibility(View.VISIBLE);
    }


    public void onEventMainThread(MenuCidEvent event) {
        if (!event.getCid().isEmpty()) {
            WebviewUtils.getInstance().callJs("changecid('" + event.getCid() + "')", mWebView);
        }
    }
}
