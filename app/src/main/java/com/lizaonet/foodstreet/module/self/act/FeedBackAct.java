package com.lizaonet.foodstreet.module.self.act;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class FeedBackAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("意见反馈");
    }

    @Override
    protected String htmlFileName() {
        return "feedback";
    }

    @Override
    protected void changeAct(String paramString) {

    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
