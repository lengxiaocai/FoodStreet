package com.lizaonet.foodstreet.module.menu.model;

public class SendFoodMaterialEvent {
    private String data;

    public SendFoodMaterialEvent(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
