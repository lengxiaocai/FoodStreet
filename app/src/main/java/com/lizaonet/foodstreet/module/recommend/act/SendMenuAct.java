package com.lizaonet.foodstreet.module.recommend.act;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.widget.Toast;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.AddfoodMaterial;
import com.lizaonet.foodstreet.module.menu.act.CookAddAct;
import com.lizaonet.foodstreet.module.menu.model.ModifyFoodMaterialEvent;
import com.lizaonet.foodstreet.module.menu.model.ModifyFoodStepEvent;
import com.lizaonet.foodstreet.module.menu.model.SendFoodMaterialEvent;
import com.lizaonet.foodstreet.module.menu.model.SendFoodStepEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.FileUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SendMenuAct extends BaseWebViewAct {

    private static final int REQUEST_PHOTO_CHOOSE = 25;
    private int MAXSIZE = 1;

    private List<String> fileList = new ArrayList<>();

    private String funName = "";
    private String index = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("发布菜谱");
    }

    @Override
    protected String htmlFileName() {
        return "sendcookbook";
    }

    @Override
    protected void changeAct(String paramString) {
        final BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "addstep":
                    Intent intent = new Intent(SendMenuAct.this, CookAddAct.class);
                    startActivity(intent);
                    break;
                case "addfoodmaterial":
                    Intent addfoodmaterialiIntent = new Intent(SendMenuAct.this, AddfoodMaterial.class);
                    addfoodmaterialiIntent.putExtra(AddfoodMaterial.MODIFYDATA, baseResponse.getData());
                    startActivity(addfoodmaterialiIntent);
                    break;
                case "uploadimg":
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            uploadPhoto();
                            funName = baseResponse.getFuncname();
                        }
                    });
                    break;
                case "modifystep":
                    Intent modifyIntent = new Intent(SendMenuAct.this, CookAddAct.class);
                    modifyIntent.putExtra(CookAddAct.MODIFYDATA, baseResponse.getData());
                    startActivity(modifyIntent);
                    index = baseResponse.getIndex();
                    break;
                default:
                    break;
            }
        }
    }


    public void onEventMainThread(ModifyFoodStepEvent event) {
        if (!event.getData().isEmpty()) {
            WebviewUtils.getInstance().callJs("modifyStep( '" + index + "', '" + event.getData() + "')", mWebView);
        }
    }

    public void onEventMainThread(SendFoodStepEvent event) {
        if (!event.getData().isEmpty()) {
            WebviewUtils.getInstance().callJs("addStep('" + event.getData() + "')", mWebView);
        }
    }

    public void onEventMainThread(ModifyFoodMaterialEvent event) {
        if (!event.getData().isEmpty()) {
            WebviewUtils.getInstance().callJs("setMaterial('" + event.getData() + "')", mWebView);
        }
    }


    public void onEventMainThread(SendFoodMaterialEvent event) {
        if (!event.getData().isEmpty()) {
            WebviewUtils.getInstance().callJs("setMaterial('" + event.getData() + "')", mWebView);
        }
    }

    @Override
    protected String callJs(String result) {
        String phone = PreferenceHelper.getString(GlobalConstants.PHONE, "");
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'phone': '" + phone + "', 'uid': '" + uid + "'})";
    }

    private void uploadPhoto() {
        RxPermissions rxPermissions = new RxPermissions(SendMenuAct.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            Matisse.from(SendMenuAct.this)
                                    .choose(MimeType.ofImage(), false)
                                    .countable(true)
                                    .capture(true)
                                    .captureStrategy(new CaptureStrategy(true, "com.lizaonet.foodstreet.fileprovider"))
                                    .maxSelectable(MAXSIZE)
                                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.imgPicker))
                                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    .thumbnailScale(0.85f)
                                    .imageEngine(new GlideEngine())
                                    .forResult(REQUEST_PHOTO_CHOOSE);
                        } else {
                            Toast.makeText(SendMenuAct.this, R.string.permission_request_denied, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PHOTO_CHOOSE && resultCode == RESULT_OK) {
            fileList.clear();
            fileList.addAll(Matisse.obtainPathResult(data));
            final String fileToBase64 = FileUtil.bitmapToString(fileList.get(0));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebviewUtils.getInstance().callJs(funName + "('" + fileList.get(0) + "','" + fileToBase64 + "')", mWebView);
                }
            });
        }
    }
}
