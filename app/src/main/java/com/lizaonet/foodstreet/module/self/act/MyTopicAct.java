package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.topic.act.ModifyTopicAct;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class MyTopicAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("我的话题");
    }

    @Override
    protected String htmlFileName() {
        return "mytopic";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "login":
                    Intent registerIntent = new Intent(MyTopicAct.this, LoginAct.class);
                    startActivity(registerIntent);
                    finish();
                    break;
                case "topicdetail":
                    Intent intent = new Intent(MyTopicAct.this, TopicDetailAct.class);
                    intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                    startActivity(intent);
                    break;
                case "modifytopic":
                    Intent modifyIntent = new Intent(MyTopicAct.this, ModifyTopicAct.class);
                    modifyIntent.putExtra(ModifyTopicAct.ID, baseResponse.getId());
                    startActivity(modifyIntent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.reload();
    }
}
