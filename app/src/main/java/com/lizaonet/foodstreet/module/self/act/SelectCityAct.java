package com.lizaonet.foodstreet.module.self.act;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class SelectCityAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("城市选择");
    }

    @Override
    protected String htmlFileName() {
        return "selectcity";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "selectcitylist":
                    PreferenceHelper.putString("selectcitylistdata", baseResponse.getData());
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    protected String callJs(String result) {
        return "setParams()";
    }
}
