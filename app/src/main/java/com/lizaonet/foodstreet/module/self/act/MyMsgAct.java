package com.lizaonet.foodstreet.module.self.act;

import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseActivity;
import com.lizaonet.foodstreet.module.self.fragment.CommentMsgFrg;
import com.lizaonet.foodstreet.module.self.fragment.SystemMsgFrg;
import com.lizaonet.foodstreet.views.NoScrollViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

public class MyMsgAct extends BaseActivity {

    private ArrayList<Fragment> listFragment;
    @BindView(R.id.viewpager)
    NoScrollViewPager viewpager;
    @BindView(R.id.rl_1)
    RelativeLayout rl_1;
    @BindView(R.id.rl_2)
    RelativeLayout rl_2;
    @BindView(R.id.rl_3)
    RelativeLayout rl_3;

    @BindView(R.id.tv_1)
    TextView tv_1;
    @BindView(R.id.tv_2)
    TextView tv_2;
    @BindView(R.id.tv_3)
    TextView tv_3;

    @BindView(R.id.view_1)
    View view_1;
    @BindView(R.id.view_2)
    View view_2;
    @BindView(R.id.view_3)
    View view_3;

    private void setFragment() {
        ConversationListFragment conversationListFragment = new ConversationListFragment();
        Uri uri = Uri.parse("rong://" + MyMsgAct.this.getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist").appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话，该会话聚合显示
                .build();
        conversationListFragment.setUri(uri);
        listFragment = new ArrayList<>();
        listFragment.add(SystemMsgFrg.getInstance());
        listFragment.add(conversationListFragment);
        listFragment.add(CommentMsgFrg.getInstance());
        viewpager.setAdapter(new MyAdapter(getSupportFragmentManager()));
    }

    @Override
    protected void initViews() {
        showTitleLeftBtn();
        setTitleMiddleText("我的消息");
        setFragment();
        rl_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatus(tv_1, view_1);
                viewpager.setCurrentItem(0);
            }
        });
        rl_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatus(tv_2, view_2);
                viewpager.setCurrentItem(1);
            }
        });
        rl_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatus(tv_3, view_3);
                viewpager.setCurrentItem(2);
            }
        });
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.activity_message;
    }

    private class MyAdapter extends FragmentPagerAdapter {

        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return listFragment.get(position);
        }

        @Override
        public int getCount() {
            return listFragment.size();
        }

    }

    private void changeStatus(TextView textView, View view) {
        tv_1.setTextColor(Color.parseColor("#333333"));
        tv_2.setTextColor(Color.parseColor("#333333"));
        tv_3.setTextColor(Color.parseColor("#333333"));
        view_1.setVisibility(View.INVISIBLE);
        view_2.setVisibility(View.INVISIBLE);
        view_3.setVisibility(View.INVISIBLE);
        textView.setTextColor(Color.parseColor("#FBBF41"));
        view.setVisibility(View.VISIBLE);
    }
}
