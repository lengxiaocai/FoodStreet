package com.lizaonet.foodstreet.module.menu.act;

import android.view.View;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.model.ModifyFoodMaterialEvent;
import com.lizaonet.foodstreet.module.menu.model.SendFoodMaterialEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;

import io.rong.eventbus.EventBus;

public class AddfoodMaterial extends BaseWebViewAct {


    public static String MODIFYDATA = "modifydata";
    private String modifydata = "";


    @Override
    protected void initConfig() {
        setTitleMiddleText("食材明细");
        showTitleRightBtn("保存", 0);
        modifydata = getIntent().getStringExtra(MODIFYDATA);
        public_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebviewUtils.getInstance().callJs("save()", mWebView);
            }
        });
    }

    @Override
    protected String htmlFileName() {
        return "addfoodmaterial";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "saveresult":
                    String data = baseResponse.getData();
                    if (modifydata != null) {
                        if (!modifydata.isEmpty()) {
                            EventBus.getDefault().post(new ModifyFoodMaterialEvent(data));
                        } else {
                            EventBus.getDefault().post(new SendFoodMaterialEvent(data));
                        }
                    }
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        if (modifydata != null) {
            if (!modifydata.isEmpty()) {
                return "setParams({'data': '" + modifydata + "'})";
            } else {
                return "setParams({'uid': '" + uid + "'})";
            }
        } else {
            return "setParams({'uid': '" + uid + "'})";
        }
    }
}