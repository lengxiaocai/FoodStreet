package com.lizaonet.foodstreet.module.menu.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.ProfileAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class RankLlistAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("排行榜");
    }

    @Override
    protected String htmlFileName() {
        return "ranklist";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "menudetail":
                    Intent menudetailIntent = new Intent(RankLlistAct.this, BookDetailAct.class);
                    menudetailIntent.putExtra(BookDetailAct.ID, baseResponse.getId());
                    startActivity(menudetailIntent);
                    break;
                case "profile":
                    Intent profileIntent = new Intent(RankLlistAct.this, ProfileAct.class);
                    profileIntent.putExtra(ProfileAct.FUID, baseResponse.getFuid());
                    startActivity(profileIntent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
