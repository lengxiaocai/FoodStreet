package com.lizaonet.foodstreet.module.topic.act;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.widget.Toast;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.ProfileAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.AccountUtil;
import com.lizaonet.foodstreet.utils.FileUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ModifyTopicAct extends BaseWebViewAct {
    public static String ID = "id";
    private String id = "";

    private static final int REQUEST_PHOTO_CHOOSE = 25;
    private int MAXSIZE = 1;

    private List<String> fileList = new ArrayList<>();

    private String funName = "";


    @Override
    protected void initConfig() {
        setTitleMiddleText("编辑话题");
        id = getIntent().getStringExtra(ID);
    }

    @Override
    protected String htmlFileName() {
        return "modifytopic";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "profile":
                    if (AccountUtil.getInstance().isLogin()) {
                        Intent profileIntent = new Intent(ModifyTopicAct.this, ProfileAct.class);
                        profileIntent.putExtra(ProfileAct.FUID, baseResponse.getFuid());
                        startActivity(profileIntent);
                    } else {
                        Tips.instance.tipShort("请先登录");
                    }
                    break;
                case "uploadimg":
                    uploadPhoto();
                    funName = baseResponse.getFuncname();
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'topic_id': '" + id + "'})";
    }

    private void uploadPhoto() {
        RxPermissions rxPermissions = new RxPermissions(ModifyTopicAct.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            Matisse.from(ModifyTopicAct.this)
                                    .choose(MimeType.ofImage(), false)
                                    .countable(true)
                                    .capture(true)
                                    .captureStrategy(new CaptureStrategy(true, "com.lizaonet.foodstreet.fileprovider"))
                                    .maxSelectable(MAXSIZE)
                                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.imgPicker))
                                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                    .thumbnailScale(0.85f)
                                    .imageEngine(new GlideEngine())
                                    .forResult(REQUEST_PHOTO_CHOOSE);
                        } else {
                            Toast.makeText(ModifyTopicAct.this, R.string.permission_request_denied, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PHOTO_CHOOSE && resultCode == RESULT_OK) {
            fileList.clear();
            fileList.addAll(Matisse.obtainPathResult(data));
            final String fileToBase64 = FileUtil.bitmapToString(fileList.get(0));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebviewUtils.getInstance().callJs(funName + "('" + fileList.get(0) + "','" + fileToBase64 + "')", mWebView);
                }
            });
        }
    }
}
