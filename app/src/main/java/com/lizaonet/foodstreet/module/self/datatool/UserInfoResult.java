package com.lizaonet.foodstreet.module.self.datatool;

import com.lizaonet.foodstreet.base.BaseResponse;

public class UserInfoResult extends BaseResponse {

    /**
     * avatar : http://106.14.83.28:63/Uploads/Picture/2018-09-19/15373267811659.png
     */

    private String head_img;
    private String username;

    public String getHead_img() {
        return head_img;
    }

    public void setHead_img(String head_img) {
        this.head_img = head_img;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
