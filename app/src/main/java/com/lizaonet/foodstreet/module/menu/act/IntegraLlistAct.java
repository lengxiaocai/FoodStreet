package com.lizaonet.foodstreet.module.menu.act;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class IntegraLlistAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("积分明细");
    }

    @Override
    protected String htmlFileName() {
        return "integrallist";
    }

    @Override
    protected void changeAct(String paramString) {
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
