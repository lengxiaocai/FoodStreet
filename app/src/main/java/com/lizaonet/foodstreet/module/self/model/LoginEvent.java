package com.lizaonet.foodstreet.module.self.model;

public class LoginEvent {

    public String update = "";

    public String getUpdate() {
        return update;
    }

    public LoginEvent(String update) {
        this.update = update;
    }

}
