package com.lizaonet.foodstreet.module.self.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseFragment;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.self.act.MyMsgDetailAct;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;

public class CommentMsgFrg extends BaseFragment {

    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    private X5WebView mWebView;

    public static CommentMsgFrg getInstance() {
        return new CommentMsgFrg();
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_base_webview;
    }

    @Override
    protected void initViews() {
        toolbar.setVisibility(View.GONE);
        initWebView();
        mWebView.setWebChromeClient(new WebChromeClient() {

            IX5WebChromeClient.CustomViewCallback callback;

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                Log.d("111111111", arg3 + "");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);

            }

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {

                callback = customViewCallback;
            }
        });

        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(getActivity());
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        setKuaYu();
        mWebView.loadUrl("file:///android_asset/appweb/html/commentmessage.html");
    }

    private void initWebView() {
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    private void setKuaYu() {

        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private void callJs(String jsString) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            mWebView.loadUrl("javascript:" + jsString);
        } else {
            mWebView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    //此处为 js 返回的结果

                    Log.d("11111111", value);
                }
            });
        }
    }

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public String GetChannel() {
            return "aaaa";
        }

        @JavascriptInterface
        public void changeActivity() {

        }


        @JavascriptInterface
        public void initParams(String result) {
            final String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callJs("setParams({'uid': '" + uid + "'})");
                }
            });

        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if (baseResponse.getTo() != null) {
                switch (baseResponse.getTo()) {
                    case "msgdetail":
                        Intent intent = new Intent(getActivity(), MyMsgDetailAct.class);
                        intent.putExtra(MyMsgDetailAct.ID, baseResponse.getId());
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void onEventMainThread(LoginEvent event) {
        if (!event.getUpdate().isEmpty()) {
            mWebView.reload();
        }
    }
}
