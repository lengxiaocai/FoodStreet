package com.lizaonet.foodstreet.module.menu.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class BookCategoryAct extends BaseWebViewAct {

    @Override
    protected void initConfig() {
        setTitleMiddleText("菜谱分类");
    }

    @Override
    protected String htmlFileName() {
        return "bookcategory";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) switch (baseResponse.getTo()) {
            case "booklist":
                Intent intent = new Intent(BookCategoryAct.this, BookListAct.class);
                intent.putExtra(BookListAct.ID, baseResponse.getC_id());
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
