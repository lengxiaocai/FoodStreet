package com.lizaonet.foodstreet.module.menu.act;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class GoodDetailAct extends BaseWebViewAct {

    public static String ID = "id";
    private String id = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("积分兑换");
        id = getIntent().getStringExtra(ID);
    }

    @Override
    protected String htmlFileName() {
        return "gooddetail";
    }

    @Override
    protected void changeAct(String paramString) {

    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'goods_id': '" + id + "'})";
    }
}
