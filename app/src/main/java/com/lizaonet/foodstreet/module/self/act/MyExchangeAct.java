package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.socks.library.KLog;

public class MyExchangeAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("我的兑换");
    }

    @Override
    protected String htmlFileName() {
        return "myexchange";
    }

    @Override
    protected void changeAct(String paramString) {
        KLog.d("----------------", paramString);
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if ("login".equals(baseResponse.getTo())) {
            Intent registerIntent = new Intent(MyExchangeAct.this, LoginAct.class);
            startActivity(registerIntent);
            finish();
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
