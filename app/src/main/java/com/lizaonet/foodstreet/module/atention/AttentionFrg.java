package com.lizaonet.foodstreet.module.atention;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseFragment;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.BookDetailAct;
import com.lizaonet.foodstreet.module.self.act.LoginAct;
import com.lizaonet.foodstreet.module.self.act.ProfileAct;
import com.lizaonet.foodstreet.module.self.event.GotoHomeEvent;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.AccountUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;

public class AttentionFrg extends BaseFragment {

    @BindView(R.id.frg_toolbar)
    Toolbar frg_toolbar;
    @BindView(R.id.ll_attentionbook)
    LinearLayout ll_attentionbook;
    @BindView(R.id.ll_attentiontopic)
    LinearLayout ll_attentiontopic;
    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    @BindView(R.id.view_topic)
    View view_topic;
    @BindView(R.id.view_book)
    View view_book;
    private X5WebView mWebView;

    public static AttentionFrg getInstance() {
        return new AttentionFrg();
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.fragment_attention;
    }

    @Override
    protected void initViews() {
        toolbar.setVisibility(View.GONE);
        initWebView();
        mWebView.setWebChromeClient(new WebChromeClient() {

            IX5WebChromeClient.CustomViewCallback callback;

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                Log.d("111111111", arg3 + "");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);

            }

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {

                callback = customViewCallback;
            }
        });
        ll_attentionbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.loadUrl("file:///android_asset/appweb/html/" + "attentionbook" + ".html");
                setParams();
                view_book.setVisibility(View.VISIBLE);
                view_topic.setVisibility(View.INVISIBLE);
            }
        });
        ll_attentiontopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.loadUrl("file:///android_asset/appweb/html/" + "attentiotopic" + ".html");
                setParams();
                view_book.setVisibility(View.INVISIBLE);
                view_topic.setVisibility(View.VISIBLE);
            }
        });
        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(getActivity());
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        setKuaYu();
        initView();
    }

    private void initView() {
        if (AccountUtil.getInstance().isLogin()) {
            toolbar.setVisibility(View.GONE);
            frg_toolbar.setVisibility(View.VISIBLE);
            mWebView.loadUrl("file:///android_asset/appweb/html/attentionbook.html");
            view_book.setVisibility(View.VISIBLE);
            view_topic.setVisibility(View.INVISIBLE);
        } else {
            frg_toolbar.setVisibility(View.GONE);
            toolbar.setVisibility(View.VISIBLE);
            setTitleMiddleText("关注");
            mWebView.loadUrl("file:///android_asset/appweb/html/unlogin.html");
        }
    }

    private void initWebView() {
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    private void setKuaYu() {

        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private void callJs(String jsString) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            mWebView.loadUrl("javascript:" + jsString);
        } else {
            mWebView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    //此处为 js 返回的结果

                    Log.d("11111111", value);
                }
            });
        }
    }

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public String GetChannel() {
            return "aaaa";
        }

        @JavascriptInterface
        public void changeActivity() {

        }


        @JavascriptInterface
        public void initParams(String result) {
            final String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callJs("setParams({'uid': '" + uid + "'})");
                }
            });

        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if (baseResponse.getTo() != null) {
                switch (baseResponse.getTo()) {
                    case "topicdetail":
                        Intent intent = new Intent(getActivity(), TopicDetailAct.class);
                        intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                        startActivity(intent);
                        break;
                    case "bookdetail":
                        Intent bookdetailIntent = new Intent(getActivity(), BookDetailAct.class);
                        bookdetailIntent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                        startActivity(bookdetailIntent);
                        break;
                    case "profile":
                        if (AccountUtil.getInstance().isLogin()) {
                            Intent profileIntent = new Intent(getActivity(), ProfileAct.class);
                            profileIntent.putExtra(ProfileAct.FUID, baseResponse.getFuid());
                            startActivity(profileIntent);
                        } else {
                            Tips.instance.tipShort("请先登录");
                        }
                        break;
                    case "nologin":
                        Intent loginIntent = new Intent(getActivity(), LoginAct.class);
                        startActivity(loginIntent);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void onEventMainThread(LoginEvent event) {
        if (!event.getUpdate().isEmpty()) {
            mWebView.reload();
        }
    }

    private void setParams() {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        WebviewUtils.getInstance().callJs("setParams({'uid': '" + uid + "'})", mWebView);
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }

    public void onEventMainThread(GotoHomeEvent event) {
        if (event.isGotoHome()) {
            initView();
        }
    }
}
