package com.lizaonet.foodstreet.module.menu.model;

public class ModifyFoodMaterialEvent {
    private String data;

    public ModifyFoodMaterialEvent(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
