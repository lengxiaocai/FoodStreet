package com.lizaonet.foodstreet.module.menu.act;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.recommend.act.InformationDetailAct;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.lizaonet.foodstreet.utils.WebviewUtils;

import butterknife.BindView;

public class SearchAct extends BaseWebViewAct {
    @BindView(R.id.tv_type)
    TextView tv_type;
    @BindView(R.id.et_search)
    EditText et_search;
    @BindView(R.id.tv_search)
    TextView tv_search;
    @BindView(R.id.ll_search_type)
    LinearLayout ll_search_type;
    @BindView(R.id.ll_back)
    RelativeLayout ll_back;


    public static String TYPE = "type";
    private String type = "";


    public static String TITLE = "title";
    private String title = "";

    @Override
    protected void initConfig() {
        mToolbar.setVisibility(View.GONE);
        type = getIntent().getStringExtra(TYPE);
        title = getIntent().getStringExtra(TITLE);
        WebviewUtils.getInstance().callJs("changeType('" + type + "')", mWebView);
        tv_type.setText(title);
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_search.getText().toString().isEmpty()) {
                    WebviewUtils.getInstance().callJs("search('" + et_search.getText().toString() + "')", mWebView);
                } else {
                    Tips.instance.tipShort("请输入搜索内容");
                }
            }
        });
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_search;
    }

    @Override
    protected String htmlFileName() {
        return "search";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "changeType":
                    tv_type.setText(baseResponse.getTypename());
                    break;
                case "topicdetail":
                    Intent intent = new Intent(SearchAct.this, TopicDetailAct.class);
                    intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                    startActivity(intent);
                    break;
                case "informationdetail":
                    Intent informationdetailIntent = new Intent(SearchAct.this, InformationDetailAct.class);
                    informationdetailIntent.putExtra(InformationDetailAct.ID, baseResponse.getId());
                    startActivity(informationdetailIntent);
                    break;
                case "menudetail":
                    Intent menudetailIntent = new Intent(SearchAct.this, BookDetailAct.class);
                    menudetailIntent.putExtra(BookDetailAct.ID, baseResponse.getId());
                    startActivity(menudetailIntent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'type': '" + type + "'})";
    }
}
