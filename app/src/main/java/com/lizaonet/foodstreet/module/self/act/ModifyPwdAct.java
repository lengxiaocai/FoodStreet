package com.lizaonet.foodstreet.module.self.act;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class ModifyPwdAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("修改密码");
    }

    @Override
    protected String htmlFileName() {
        return "modifypwd";
    }

    @Override
    protected void changeAct(String paramString) {

    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        String mobile = PreferenceHelper.getString(GlobalConstants.PHONE, "");
        return "setParams({'uid': '" + uid + "', 'mobile': '" + mobile + "'})";
    }
}
