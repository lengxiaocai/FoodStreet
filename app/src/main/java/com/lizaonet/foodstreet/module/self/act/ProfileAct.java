package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.BookDetailAct;
import com.lizaonet.foodstreet.module.topic.act.TopicDetailAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

import io.rong.imkit.RongIM;

public class ProfileAct extends BaseWebViewAct {

    public static String FUID = "fuid";
    private String fuid = "";

    @Override
    protected void initConfig() {
        setTitleMiddleText("个人主页");
        fuid = getIntent().getStringExtra(FUID);
    }

    @Override
    protected String htmlFileName() {
        return "profile";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "chat":
                    RongIM.getInstance().startPrivateChat(ProfileAct.this, baseResponse.getChatuid(), baseResponse.getChatname());
                    break;
                case "topicdetail":
                    Intent intent = new Intent(ProfileAct.this, TopicDetailAct.class);
                    intent.putExtra(TopicDetailAct.ID, baseResponse.getId());
                    startActivity(intent);
                    break;
                case "menudetail":
                    Intent menudetailIntent = new Intent(ProfileAct.this, BookDetailAct.class);
                    menudetailIntent.putExtra(BookDetailAct.ID, baseResponse.getId());
                    startActivity(menudetailIntent);
                    break;
                default:
                    break;
            }
        }

    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "', 'f_uid': '" + fuid + "'})";
    }
}
