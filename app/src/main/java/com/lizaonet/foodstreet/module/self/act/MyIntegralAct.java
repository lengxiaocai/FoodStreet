package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.IntegralExchangeAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class MyIntegralAct extends BaseWebViewAct {

    @Override
    protected void initConfig() {
        setTitleMiddleText("我的积分");
    }

    @Override
    protected String htmlFileName() {
        return "myintegrallist";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "integralexchange":
                    Intent intent = new Intent(MyIntegralAct.this, IntegralExchangeAct.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}