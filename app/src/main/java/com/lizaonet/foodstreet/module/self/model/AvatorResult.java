package com.lizaonet.foodstreet.module.self.model;

public class AvatorResult {

    /**
     * result : 1
     * error : 头像修改成功！
     * url : http://101.132.102.134/wap/JSON/Forum/Avatar/1743/default.jpg
     */

    private String result;
    private String error;
    private String url;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
