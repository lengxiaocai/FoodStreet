package com.lizaonet.foodstreet.module.recommend.model;

public class UpdateEvent {
    private String link_url;

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public UpdateEvent(String link_url) {

        this.link_url = link_url;
    }
}
