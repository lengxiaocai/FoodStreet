package com.lizaonet.foodstreet.module.self.act;

import android.view.View;

import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.utils.WebviewUtils;

public class ForgotPwdAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("忘记密码");
        showTitleRightBtn("确定", 0);
        public_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebviewUtils.getInstance().callJs("reset()", mWebView);
            }
        });
    }

    @Override
    protected String htmlFileName() {
        return "forgotpwd";
    }

    @Override
    protected void changeAct(String paramString) {

    }

    @Override
    protected String callJs(String result) {
        return "";
    }
}
