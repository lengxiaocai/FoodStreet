package com.lizaonet.foodstreet.module.menu.model;

public class ModifyFoodStepEvent {
    private String data;

    public ModifyFoodStepEvent(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
