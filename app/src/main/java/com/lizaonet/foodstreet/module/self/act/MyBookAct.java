package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.BookDetailAct;
import com.lizaonet.foodstreet.module.menu.act.ModifyMenuAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class MyBookAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("我的菜谱");
    }

    @Override
    protected String htmlFileName() {
        return "mybook";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "menudetail":
                    Intent menudetailIntent = new Intent(MyBookAct.this, BookDetailAct.class);
                    menudetailIntent.putExtra(BookDetailAct.ID, baseResponse.getId());
                    startActivity(menudetailIntent);
                    break;
                case "modifymenu":
                    Intent modifyIntent = new Intent(MyBookAct.this, ModifyMenuAct.class);
                    modifyIntent.putExtra(ModifyMenuAct.ID, baseResponse.getId());
                    startActivity(modifyIntent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.reload();
    }
}
