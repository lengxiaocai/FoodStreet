package com.lizaonet.foodstreet.module.self.act;

import android.net.Uri;
import android.view.View;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseActivity;

import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

public class PrivateChatAct extends BaseActivity {

    private ConversationListFragment conversationlist;

    @Override
    protected void initViews() {
        showTitleLeftBtn();
        mToolbar.setVisibility(View.GONE);
        Uri uri = Uri.parse("rong://" + PrivateChatAct.this.getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist").appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话，该会话聚合显示
                .build();
        conversationlist.setUri(uri);
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.act_private_chat;
    }
}
