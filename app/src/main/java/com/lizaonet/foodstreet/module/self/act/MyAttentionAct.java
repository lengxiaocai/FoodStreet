package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.topic.act.ModifyTopicAct;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.AccountUtil;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.Tips;
import com.socks.library.KLog;

public class MyAttentionAct extends BaseWebViewAct {

    @Override
    protected void initConfig() {
        setTitleMiddleText("关注的人");
    }

    @Override
    protected String htmlFileName() {
        return "attentionlist";
    }

    @Override
    protected void changeAct(String paramString) {
        KLog.d("----------------", paramString);
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if ("profile".equals(baseResponse.getTo())) {
            if (AccountUtil.getInstance().isLogin()) {
                Intent intent = new Intent(MyAttentionAct.this, ProfileAct.class);
                intent.putExtra(ProfileAct.FUID, baseResponse.getFuid());
                startActivity(intent);
            } else {
                Tips.instance.tipShort("请先登录");
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
