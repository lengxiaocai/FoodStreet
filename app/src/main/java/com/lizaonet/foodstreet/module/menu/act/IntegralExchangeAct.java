package com.lizaonet.foodstreet.module.menu.act;

import android.content.Intent;
import android.view.View;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;

public class IntegralExchangeAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("积分兑换");
        showTitleRightBtn("积分明细", 0);
        public_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntegralExchangeAct.this, IntegraLlistAct.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected String htmlFileName() {
        return "integralexchange";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "gooddetail":
                    Intent intent = new Intent(IntegralExchangeAct.this, GoodDetailAct.class);
                    intent.putExtra(GoodDetailAct.ID, baseResponse.getId());
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'uid': '" + uid + "'})";
    }
}
