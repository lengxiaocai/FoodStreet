package com.lizaonet.foodstreet.module.self.act;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.lizaonet.foodstreet.MainActivity;
import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.base.CommonWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.Map;

import butterknife.BindView;

public class LoginAct extends BaseWebViewAct {
    private String TAG = this.getClass().getSimpleName();

    @BindView(R.id.ll_finsh)
    RelativeLayout ll_finsh;

    @BindView(R.id.login_toolbar)
    Toolbar login_toolbar;

    private int type = 0;

    @Override
    protected void initConfig() {
        mToolbar.setVisibility(View.GONE);
        login_toolbar.setVisibility(View.VISIBLE);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setTitleMiddleText("");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (!TextUtils.isEmpty(PreferenceHelper.getString(GlobalConstants.UID, ""))) {
            startActivity(new Intent(LoginAct.this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected String htmlFileName() {
        return "loginandregister";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if ("forgotpwd".equals(baseResponse.getTo())) {
            Intent registerIntent = new Intent(LoginAct.this, ForgotPwdAct.class);
            startActivity(registerIntent);
        } else if ("agreement".equals(baseResponse.getTo())) {
            Intent agreementIntent = new Intent(LoginAct.this, CommonWebViewAct.class);
            agreementIntent.putExtra(CommonWebViewAct.URL, baseResponse.getLink_url());
            agreementIntent.putExtra(CommonWebViewAct.TITLE, "注册协议");
            startActivity(agreementIntent);
        } else if ("thirdlogin".equals(baseResponse.getTo())) {
            if ("qq".equals(baseResponse.getType())) {
                type = 2;
                authorization(SHARE_MEDIA.QQ);
            } else if ("sina".equals(baseResponse.getType())) {
                type = 4;
                authorization(SHARE_MEDIA.SINA);
            } else if ("wx".equals(baseResponse.getType())) {
                type = 3;
                authorization(SHARE_MEDIA.WEIXIN);
            }
        }
    }

    @Override
    protected String callJs(String result) {
        return "";
    }


    //授权
    private void authorization(SHARE_MEDIA share_media) {
        UMShareAPI.get(this).getPlatformInfo(this, share_media, umAuthListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            Log.d(TAG, "onStart " + "授权开始");
        }

        @Override
        public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
            Log.d(TAG, "onComplete " + "授权完成");
            if (map != null) {
                //sdk是6.4.4的,但是获取值的时候用的是6.2以前的(access_token)才能获取到值,未知原因
                String uid = map.get("uid");
                String openid = map.get("openid");//微博没有
                String access_token = map.get("access_token");
                String refresh_token = map.get("refresh_token");//微信,qq,微博都没有获取到
                String expires_in = map.get("expires_in");
                String gender = map.get("gender");
                String name = map.get("name");
                String iconurl = map.get("iconurl");
                String unionid = map.get("unionid");//微博没有
                //拿到信息去请求登录接口。。。
                WebviewUtils.getInstance().callJs("thirdlogin('" + uid + "','" + name + "','" + iconurl + "','" + type + "')", mWebView);
            }
        }

        @Override
        public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
            Log.d(TAG, "onError " + "授权失败");
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media, int i) {
            Log.d(TAG, "onCancel " + "授权取消");
        }
    };
}