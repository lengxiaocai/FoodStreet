package com.lizaonet.foodstreet.module.self.act;

import android.view.View;

import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.base.BaseWebViewAct;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.WebviewUtils;

public class BindPhoneAct extends BaseWebViewAct {
    @Override
    protected void initConfig() {
        setTitleMiddleText("绑定手机");
        showTitleRightBtn("确定", 0);
        public_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                public_title_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        WebviewUtils.getInstance().callJs("modify()", mWebView);
                    }
                });
            }
        });
    }

    @Override
    protected String htmlFileName() {
        return "bindphone";
    }

    @Override
    protected void changeAct(String paramString) {
        BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
        if (baseResponse.getTo() != null) {
            switch (baseResponse.getTo()) {
                case "savephone":
                    PreferenceHelper.putString(GlobalConstants.PHONE, baseResponse.getPhone());
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected String callJs(String result) {
        String phone = PreferenceHelper.getString(GlobalConstants.PHONE, "");
        String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
        return "setParams({'phone': '" + phone + "', 'uid': '" + uid + "'})";
    }
}
