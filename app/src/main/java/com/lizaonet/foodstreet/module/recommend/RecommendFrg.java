package com.lizaonet.foodstreet.module.recommend;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.lizaonet.foodstreet.R;
import com.lizaonet.foodstreet.app.App;
import com.lizaonet.foodstreet.base.BaseFragment;
import com.lizaonet.foodstreet.base.BaseResponse;
import com.lizaonet.foodstreet.config.GlobalConstants;
import com.lizaonet.foodstreet.module.menu.act.SearchAct;
import com.lizaonet.foodstreet.module.recommend.act.InformationDetailAct;
import com.lizaonet.foodstreet.module.recommend.model.UpdateEvent;
import com.lizaonet.foodstreet.module.self.model.LoginEvent;
import com.lizaonet.foodstreet.net.JSONUtils;
import com.lizaonet.foodstreet.utils.PreferenceHelper;
import com.lizaonet.foodstreet.utils.StringUtils;
import com.lizaonet.foodstreet.utils.WebviewUtils;
import com.lizaonet.foodstreet.utils.X5WebView;
import com.socks.library.KLog;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;
import io.rong.eventbus.EventBus;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;

public class RecommendFrg extends BaseFragment {

    @BindView(R.id.ll_search_information)
    LinearLayout ll_search_information;
    @BindView(R.id.mViewParent)
    ViewGroup mViewParent;
    private X5WebView mWebView;

    public static RecommendFrg getInstance() {
        return new RecommendFrg();
    }

    @Override
    protected int getChildInflateLayout() {
        return R.layout.frg_recommend;
    }

    @Override
    protected void initViews() {
        toolbar.setVisibility(View.GONE);
        initWebView();
        showTitleRightBtn();
        String token = PreferenceHelper.getString(GlobalConstants.TOKEN, "");
        if (!token.isEmpty()) {
            connect(getActivity(), token);
        }
        mWebView.setWebChromeClient(new WebChromeClient() {

            IX5WebChromeClient.CustomViewCallback callback;

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2,
                                     JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                Log.d("111111111", arg3 + "");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);

            }

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {

                callback = customViewCallback;
            }
        });

        NewsJavaScriptInterface newsJavaScriptInterface = new NewsJavaScriptInterface(getActivity());
        mWebView.addJavascriptInterface(newsJavaScriptInterface, "NewsInterface");
        setKuaYu();
        mWebView.loadUrl("file:///android_asset/appweb/html/information.html");
        ll_search_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchAct.class);
                intent.putExtra(SearchAct.TYPE, "3");
                intent.putExtra(SearchAct.TITLE, "资讯");
                startActivity(intent);
            }
        });
    }

    private void initWebView() {
        mWebView = new X5WebView(getActivity(), null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
    }

    private void setKuaYu() {

        try {
            if (Build.VERSION.SDK_INT >= 16) {
                Class<?> clazz = mWebView.getSettings().getClass();
                Method method = clazz.getMethod(
                        "setAllowUniversalAccessFromFileURLs", boolean.class);
                if (method != null) {
                    method.invoke(mWebView.getSettings(), true);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private void callJs(String jsString) {
        // Android版本变量
        final int version = Build.VERSION.SDK_INT;
        // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
        if (version < 18) {
            mWebView.loadUrl("javascript:" + jsString);
        } else {
            mWebView.evaluateJavascript("javascript:" + jsString, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    //此处为 js 返回的结果

                    Log.d("11111111", value);
                }
            });
        }
    }

    public class NewsJavaScriptInterface {
        Context mContext;

        NewsJavaScriptInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public String GetChannel() {
            return "aaaa";
        }

        @JavascriptInterface
        public void changeActivity() {

        }


        @JavascriptInterface
        public void initParams(String result) {
            final String uid = PreferenceHelper.getString(GlobalConstants.UID, "");
            final String versionCode = StringUtils.getLocalVersionName(getActivity());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callJs("setParams({'uid': '" + uid +  "', 'version': '" + versionCode + "'})");
                }
            });

        }

        @JavascriptInterface
        public void changeActivity(String paramString) {
            KLog.d("----------------", paramString);
            BaseResponse baseResponse = JSONUtils.readJSONToObject(paramString, BaseResponse.class);
            if (baseResponse.getTo() != null) {
                switch (baseResponse.getTo()) {
                    case "informationdetail":
                        Intent informationdetailIntent = new Intent(getActivity(), InformationDetailAct.class);
                        informationdetailIntent.putExtra(InformationDetailAct.ID, baseResponse.getId());
                        startActivity(informationdetailIntent);
                        break;
                    case "gettoken":
                        PreferenceHelper.putString(GlobalConstants.TOKEN, baseResponse.getToken());
                        connect(getActivity(), baseResponse.getToken());
                        break;
                    case "update":
                        EventBus.getDefault().post(new UpdateEvent(baseResponse.getLink_url()));
                    default:
                        break;
                }
            }
        }
    }

    public void onEventMainThread(LoginEvent event) {
        if (!event.getUpdate().isEmpty()) {
            mWebView.reload();
        }
    }

    public void connect(Context context, String token) {
        if (context.getApplicationInfo().packageName.equals(App.getCurProcessName(context.getApplicationContext()))) {
            RongIM connect = RongIM.connect(token, new RongIMClient.ConnectCallback() {
                /**
                 * Token 错误，在线上环境下主要是因为 Token 已经过期，您需要向 App Server 重新请求一个新的 Token
                 */
                @Override
                public void onTokenIncorrect() {
                    WebviewUtils.getInstance().callJs("getToken()", mWebView);
                }

                /**
                 * 连接融云成功
                 * @param userid 当前 token
                 */
                @Override
                public void onSuccess(String userid) {
                    //连接融云成功以后要调用setCurrentUserInfo方法在融云需要的地方显示当前用户的头像。
                    String username = PreferenceHelper.getString(GlobalConstants.USERNAME, "");
                    String avator = PreferenceHelper.getString(GlobalConstants.HEAD, "");
                    RongIM.getInstance().setCurrentUserInfo(new UserInfo(userid, username, Uri.parse(avator)));
                    //为了显示陌生人的头像，携带到消息中
                    RongIM.getInstance().setMessageAttachedUserInfo(false);
                    RongIM.getInstance().enableNewComingMessageIcon(true);//显示新消息提醒
                    RongIM.getInstance().enableUnreadMessageIcon(true);//显示未读消息数目
                    PreferenceHelper.putString(GlobalConstants.RONGIMUID, userid);
                }

                /**
                 * 连接融云失败
                 * @param errorCode 错误码，可到官网 查看错误码对应的注释
                 *                  http://www.rongcloud.cn/docs/android.html#常见错误码
                 */
                @Override
                public void onError(RongIMClient.ErrorCode errorCode) {

                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String token = PreferenceHelper.getString(GlobalConstants.TOKEN, "");
        if (!token.isEmpty()) {
            connect(getActivity(), token);
        }
    }
}
