package com.lizaonet.foodstreet.app;

/**
 * Created by caoxiuyun on 2017/10/18.
 */

public class NotificationResutl {

    /**
     * uid : 3263
     * stype : 7
     * s_type : 0
     * id : 42302
     * status : 1
     */

    private String uid;
    private int stype;
    private int s_type;
    private String id;
    private int status;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getStype() {
        return stype;
    }

    public void setStype(int stype) {
        this.stype = stype;
    }

    public int getS_type() {
        return s_type;
    }

    public void setS_type(int s_type) {
        this.s_type = s_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
