package com.lizaonet.foodstreet.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.lizaonet.foodstreet.MainActivity;
import com.socks.library.KLog;
import com.tencent.smtt.sdk.QbSdk;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;

import java.util.ArrayList;
import java.util.List;

import io.rong.imkit.RongIM;
import io.rong.imlib.model.UserInfo;


/**
 * Application
 */
public class App extends MultiDexApplication implements RongIM.UserInfoProvider {
    private static App instance;
    /**
     * 用来记录打开过的activity
     */
    private List<Activity> acts = new ArrayList<Activity>();

    /**
     * 判断当前应用程序处于前台还是后台
     */
    public static boolean isApplicationBroughtToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取应用上下文对象
     */
    public static Context getContext() {
        return getInstance().getApplicationContext();
    }

    /**
     * 获取application
     */
    public static App getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }


    static {
        //微信
        PlatformConfig.setWeixin("wx618d240b2fb17262", "a1ff86fb3f9ff09fb4413d3e450ada92");
        //新浪微博(第三个参数为回调地址)
        PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad", "http://sns.whalecloud.com/sina2/callback");
        //QQ
        PlatformConfig.setQQZone("101517443", "40c108b557b2eab861a6748e85369a79");
    }


    public List<Activity> getActs() {
        return acts;
    }

    public void setActs(List<Activity> acts) {
        this.acts = acts;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        RongIM.init(this, "x4vkb1qpxfekk");
        UMShareAPI.get(this);//初始化sdk
        UMConfigure.init(instance, "5be2baf6f1f5563c5f000210", "app", UMConfigure.DEVICE_TYPE_PHONE, "");
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onCoreInitFinished() {
                // TODO Auto-generated method stub
            }

            @Override
            public void onViewInitFinished(boolean arg0) {
                KLog.d("X5浏览器初始", arg0 ? "成功" : "失败");
                // TODO Auto-generated method stub
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }

    /**
     * 把打开的activity添加进集合
     */
    public void addActivity(Activity activity) {
        acts.add(activity);
    }

    /**
     * 从集合中移除activity
     *
     * @param activity 当前的activity
     */
    public void removeActivity(Activity activity) {
        acts.remove(activity);
    }

    /**
     * 当前activity的大小
     */
    public int getCurrentActivitySize() {
        return acts.size();
    }

    /**
     * 应用退出的时候清除掉未销毁的activity
     */
    public void cleanActivity() {
        int size = acts.size();
        for (int i = 0; i < size; i++) {
            Activity activity = acts.remove(0);
            if (activity instanceof MainActivity) {
                acts.add(activity);
                continue;
            }
            activity.finish();
        }
    }

    public void cleanAllActivity() {
        for (int i = 0; i < acts.size(); i++) {
            Activity activity = acts.get(i);
            activity.finish();
        }
    }

    /**
     * 查看是否包含这个activity
     */
    public boolean containsAct(Class clazz) {
        int size = acts.size();
        for (int i = 0; i < size; i++) {
            Activity act = acts.get(i);
            if (act.getClass().getSimpleName().equals(clazz.getSimpleName())) {
                return true;
            }
        }
        return false;
    }

    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    @Override
    public UserInfo getUserInfo(String s) {
        return null;
    }
}
