package com.lizaonet.foodstreet.net;


import java.util.HashMap;
import java.util.Map;

/**
 * 数据参数处理的工具类
 */
public class RequestUtil {
    /**
     * 处理请求参数
     */
    public static Map<String, String> getParams(Map<String, String> paramsMap) {
        Map<String, String> mParams = new HashMap<>();
        if (paramsMap != null) {
            mParams.putAll(paramsMap);
        } else {
            paramsMap = new HashMap<>();
        }
        return paramsMap;
    }

}
