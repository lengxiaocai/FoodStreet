package com.lizaonet.foodstreet.net;


import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSONUtils {

    /**
     * 通过gson来解析json字符串，并封装到对象中
     *
     * @param jsonString 得到的json字符串
     * @param cls        要将json数据封装到的对象字节码
     * @return
     */
    public static <T> T readJSONToObject(String jsonString, Class<T> cls) {
        T t = null;
        try {
            t = GsonMapper.getInstance().fromJson(jsonString, cls);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 通过gson将json字符串转化为对象，并存入到list中
     *
     * @param jsonString 要解析的json字符串
     * @param cls        封装的对象字节码
     * @return
     */
    public static <T> List<T> readJSONToObjectList(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            list = GsonMapper.getInstance().fromJson(jsonString, new TypeToken<List<T>>() {
            }.getType());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public static Map<String, String> json2Map(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return new Gson().fromJson(str, new TypeToken<Map<String, Object>>() {
        }.getType());
    }


    public static <T> String readObject2Json(List<T> list) {
        return GsonMapper.getInstance().toJson(list);
    }

}
