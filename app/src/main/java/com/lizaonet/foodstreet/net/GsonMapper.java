package com.lizaonet.foodstreet.net;


import com.google.gson.Gson;

public class GsonMapper {

    private static Gson gson = new Gson();

    private GsonMapper() {

    }

    public static Gson getInstance() {
        return gson;
    }
}
