package com.lizaonet.foodstreet.net;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.lizaonet.foodstreet.app.App;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * volley请求网络的工具类
 */
public class VolleyUtil {
    private static VolleyUtil volleyUtil;

    private KProgressHUD mLoadingDialog;
    private RequestQueue mQueue;
    private Context mContext = null;

    private VolleyUtil() {
        mQueue = Volley.newRequestQueue(App.getContext());
    }

    public static VolleyUtil getInstance() {
        if (volleyUtil == null) {
            synchronized (VolleyUtil.class) {
                if (volleyUtil == null) {
                    volleyUtil = new VolleyUtil();
                }
            }
        }
        return volleyUtil;
    }

    /**
     * post请求
     *
     * @param context  Activity类型，不需要显示Dialog
     * @param url      请求路径
     * @param params   请求参数 Map集合
     * @param objClass 对象字节码
     * @param callBack 加载完成回调
     */
    public <T> void post(boolean needDialog, Context context, String url, Map<String, String> params, Class<T> objClass, VolleyCallBack<T> callBack) {
        requestData(needDialog, Request.Method.POST, context, url, params, objClass, callBack);
    }

    /**
     * get请求
     *
     * @param context  Activity类型，不需要显示Dialog 传 null
     * @param url      请求路径
     * @param params   请求参数 Map集合
     * @param objClass 对象字节码
     * @param callBack 加载完成回调
     */
    public <T> void get(boolean needDialog, Context context, String url, Map<String, String> params, Class<T> objClass, VolleyCallBack<T> callBack) {
        requestData(needDialog, Request.Method.GET, context, getUrl(url, params), null, objClass, callBack);
    }

    /**
     * 请求数据
     *
     * @param method   get or post
     * @param url      请求路径
     * @param params   请求参数 Map集合
     * @param objClass 对象字节码
     * @param callBack 加载完成回调
     */
    public <T> void requestData(boolean needDialog, int method, final Context context, String url, Map<String, String> params, Class<T> objClass, final VolleyCallBack<T> callBack) {
        GsonRequest<T> request = null;
        if (needDialog) {
            if (mContext != context && null != mContext) {
                mLoadingDialog = KProgressHUD.create(context);
                mLoadingDialog.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("正在加载")
                        .setCancellable(true)
                        .setSize(120, 120);
            }
            if (null == mLoadingDialog || mContext != context) {
                mLoadingDialog = KProgressHUD.create(context);
                mLoadingDialog.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("正在加载")
                        .setCancellable(true)
                        .setSize(120, 120);
            }
            mContext = context;
            mLoadingDialog.show();
            request = new GsonRequest<T>(method, url, params, objClass, new Listener<T>() {
                @Override
                public void onResponse(T response) {
                    if (mLoadingDialog != null) {
                        mLoadingDialog.dismiss();
                        mLoadingDialog = null;
                    }
                    if (null != response) {
                        callBack.loadSucceed(response);
                    } else {
                        callBack.loadFailed(null);
                    }
                }
            }, new ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    if (mLoadingDialog != null) {
                        mLoadingDialog.dismiss();
                        mLoadingDialog = null;
                    }
                    callBack.loadFailed(error);
                }
            });
        } else {
            request = new GsonRequest<T>(method, url, params, objClass, new Listener<T>() {
                @Override
                public void onResponse(T response) {
                    callBack.loadSucceed(response);
                }
            }, new ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    callBack.loadFailed(error);
                }
            });
        }
        request.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, 0f));
        request.setTag(url);
        mQueue.add(request);
    }

    public RequestQueue getmQueue() {
        return mQueue;
    }

    public void cancelDataRequet(String tag) {
        mQueue.cancelAll(tag);
    }

    public String getUrl(String url, Map<String, String> params) {
        StringBuilder sb = new StringBuilder(url);
        sb.append("&");
        for (String key : params.keySet()) {
            sb.append(key);
            sb.append("=");
            try {
                sb.append(URLEncoder.encode(params.get(key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            sb.append("&");
        }
        return sb.substring(0, sb.length() - 1);
    }

    /**
     * 请求数据
     *
     * @param url      请求路径
     * @param params   请求参数 Map集合
     * @param objClass 对象字节码
     * @param callBack 加载完成回调
     */
    public <T> void getSinaData(boolean needDialog, Context context, String url, Map<String, String> params, Class<T> objClass, final VolleyCallBack<T> callBack) {
        requestData(needDialog, Request.Method.POST, context, url, params, objClass, callBack);
    }
}
