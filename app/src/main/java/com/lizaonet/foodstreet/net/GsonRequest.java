package com.lizaonet.foodstreet.net;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.lizaonet.foodstreet.config.ConfigServerInterface;
import com.socks.library.KLog;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class GsonRequest<T> extends Request<T> {
    private final Response.Listener<T> mListener;

    private Class<T> mClass;

    private Map<String, String> mParams = new HashMap<>();

    GsonRequest(int method, String url, Map<String, String> params, Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mClass = clazz;
        mListener = listener;
        mParams = params;
        //添加公共请求参数
//        mParams.put("key", MD5.encrypt(dateNowStr + "foods"));
        if (ConfigServerInterface.isDebug) {
            KLog.i("VolleyUtil.requestData", url + "     " + mParams);
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (this.mParams == null) {
            return super.getParams();
        } else {
            return mParams;
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, "UTF-8");
            if (ConfigServerInterface.isDebug) {
                KLog.json(jsonString);
            }
            return Response.success(JSONUtils.readJSONToObject(jsonString, mClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }
}
