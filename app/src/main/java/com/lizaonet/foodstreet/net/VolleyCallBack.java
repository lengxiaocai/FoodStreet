package com.lizaonet.foodstreet.net;

import com.android.volley.VolleyError;

public interface VolleyCallBack<T> {
    /**
     * 请求数据成功之后的回调
     *
     * @param result 返回对象
     */
    void loadSucceed(T result);

    /**
     * 请求数据失败之后的回调
     *
     * @param error 错误信息
     */
    void loadFailed(VolleyError error);

}
